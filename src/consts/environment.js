export const X_API_KEY = '5fe8cd60'
export const API_URL = 'https://my.api.mockaroo.com'
export const CACHE_TIME = 180
export const FLIC_R = 'https://api.flickr.com/services/rest'
export const Backend_Microservice = 'https://localhost:44381/api/'

export const EditMode = { add: 1, edit: 2 }
export const UserRole = { Admin: 1, User: 2 }
