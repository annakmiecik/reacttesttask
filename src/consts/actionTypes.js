export const ADD_TRANSACTION = 'add_transaction'
export const CREATE_BANK = 'create_bank'
export const CREATE_JAR = 'create_jar'
export const SET_BANK = 'set_bank'
export const SET_CURRENCY = 'set_currency'
export const SET_CURRENCY_FOR_NEW_JAR = 'set_currency_for_new_jar'
export const SET_NEW_BANK = 'set_new_bank'
export const SET_TARGET_BANK = 'set_target_bank'

export const WINDOW_MODE = 'window_mode'
export const WINDOW_IS_OPENED = 'window_is_opened'
export const WINDOW_SELECTED = 'window_selected'
export const WINDOW_RUN_CIRCULAR = 'window_run_circular'
