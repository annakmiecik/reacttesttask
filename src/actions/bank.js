import moment from 'moment'
import * as types from '../consts/actionTypes'

export const setCurrency = currency => (dispatch /* , getState */) =>
  dispatch({
    type: types.SET_CURRENCY,
    payload: currency
  })

export const setCurrencyForNewJar = currency => dispatch =>
  dispatch({
    type: types.SET_CURRENCY_FOR_NEW_JAR,
    payload: currency
  })

export const setBank = bank => dispatch =>
  dispatch({
    type: types.SET_BANK,
    payload: bank
  })

export const setTargetBank = bank => dispatch =>
  dispatch({
    type: types.SET_TARGET_BANK,
    payload: bank
  })

export const createBank = () => dispatch =>
  dispatch({
    type: types.CREATE_BANK
  })

export const handleChange = event => dispatch =>
  dispatch({
    type: types.SET_NEW_BANK,
    payload: event.target.value
  })

export const addTransaction = (value, multiplier, bankName) => dispatch =>
  dispatch({
    type: types.ADD_TRANSACTION,
    payload: {
      bankName: bankName,
      amount: multiplier * Math.abs(value),
      date: moment().format('MM/DD/YYYY')
    }
  })
