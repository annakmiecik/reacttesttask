import axios from 'axios'
import { FLIC_R } from '../consts/environment'

const config = () => {
  const conf = {
    baseURL: FLIC_R,
    timeout: 200000
  }
  return conf
}

export const api = axios.create(config())

export const get = (url, params) => {
  let conf = config()
  conf = { ...conf, ...params }
  return config.get(FLIC_R + url, conf)
}

export const getDogsPicture = () =>
  api
    .get(
      `/?method=flickr.photos.search&api_key=10c334a63cae65843878976469c11b76&text=dogs&has_geo=1&format=json&nojsoncallback=1&per_page=10`
    )
    .then(response => {
      if (response && response.status === 200 && response.data) {
        return response.data
      }
      throw new Error('Data In request not found')
    })

export const getDogsPictureGeo = photoID =>
  api
    .get(
      `/?method=flickr.photos.geo.getLocation&api_key=9444e41b524348a22700a37785019344&photo_id=${photoID}&format=json&nojsoncallback=1`
    )
    .then(response => {
      if (response && response.status === 200 && response.data) {
        return response.data
      }
      throw new Error('Data In request not found')
    })
