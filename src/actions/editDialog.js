import moment from 'moment'
import * as types from '../consts/actionTypes'
import { UserRole } from '../consts/environment'

export const setIsOpened = isOpened => dispatch =>
  dispatch({
    type: types.WINDOW_IS_OPENED,
    payload: isOpened
  })

export const setSelected = selected => dispatch =>
  dispatch({
    type: types.WINDOW_SELECTED,
    payload: selected
      ? selected
      : {
          id: '',
          email: '',
          username: '',
          fullName: '',
          role: UserRole.User
        }
  })

export const setRunCircular = runCircular => dispatch =>
  dispatch({
    type: types.WINDOW_RUN_CIRCULAR,
    payload: runCircular
  })

export const setMode = mode => dispatch =>
  dispatch({
    type: types.WINDOW_MODE,
    payload: mode
  })
