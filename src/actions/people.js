import { api } from '../utils/api'

export const getPeople = () => {
  return api.get(`/people`).then(response => {
    if (response && response.status === 200 && response.data) {
      return response.data
    }
    throw new Error('Data In request not found')
  })
}

export const getPeopleNoCache = () =>
  api.apiRefresh.get(`${api.apiUrl}/people`, api.refreshConfig()).then(response => {
    if (response && response.status === 200 && response.data) {
      return response.data
    }
    throw new Error('Data In request not found')
  })

export const getPerson = id =>
  api.get(`/people/${id}`).then(
    response => {
      if (response && response.status === 200 && response.data) {
        return response.data
      }
      throw new Error('Data In request not found')
    },
    error => {
      // dispatch(
      //   showErrorMessage(error.response.data.error.exception, error.response.data.error.message)
      // )
      console.log(error.response.data.error.exception)
      throw new Error('Data In request not found')
    }
  )
