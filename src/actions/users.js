import { backend_Microservice_Api } from '../utils/api'
import { handleResponse } from '../utils/authHeader'

export const getAll = logout => {
  return backend_Microservice_Api(logout)
    .get(`user`)
    .then(response => {
      if (response && response.status === 200 && response.data) {
        return response.data
      }
      throw new Error('Data In request not found')
    })
}

export const get = (username, logout) => {
  return backend_Microservice_Api(logout)
    .get(`user/${username}`)
    .then(response => {
      if (response && response.status === 200 && response.data) {
        return response.data
      }
      throw new Error('Data In request not found')
    })
}

export const add = (user, logout) => {
  return backend_Microservice_Api(logout)
    .post(`user`, user)
    .then(response => {
      if (response && response.status === 201) {
        return response.status
      }
      throw new Error('Data In request not found')
    })
}

export const update = (user, logout) => {
  return backend_Microservice_Api(logout)
    .put(`user`, user)
    .then(response => {
      if (response && response.status === 200) {
        return response.status
      }
      throw new Error('Data In request not found')
    })
}

export const del = (id, logout) => {
  return backend_Microservice_Api(logout)
    .del(`user/${id}`)
    .then(response => {
      if (response && response.status === 200) {
        return response.status
      }
      throw new Error('Data In request not found')
    })
}
