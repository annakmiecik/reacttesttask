import { backend_Microservice_Api } from '../utils/api'
import { saveUserInfo } from '../utils/localstorage'

export const fakeAuth = {
  async authenticateAsync(login, password) {
    //setTimeout(cb, 100) // fake async

    let user = await backend_Microservice_Api().put('account/login', {
      login: login,
      password: password
    })

    if (user && user.status === 200 && user.data && user.data.accessToken) {
      saveUserInfo(user.data)
      return user.data
    }

    //if (login === 'ankm' && password === 'secret') return true
    saveUserInfo(undefined)
    return undefined
  },

  signOut(cb) {
    //setTimeout(cb, 100) // fake async
    saveUserInfo(undefined)
    return undefined
  }
}
