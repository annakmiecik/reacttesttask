export const getRecordEntity = (state, bankKey) => (bankKey ? state.bank.entities[bankKey] : null)
