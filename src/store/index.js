// eslint-disable-next-line no-unused-vars
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import reducers from '../reducers/index'
import { routerMiddleware } from 'react-router-redux'

const storeConfig = browserHistory => {
  const reduxRouterMiddleware = routerMiddleware(browserHistory)

  const middlewares = [thunk, reduxRouterMiddleware]

  const createStoreWithMiddleware = compose(
    applyMiddleware(...middlewares),
    typeof window === 'object' && typeof window.devToolsExtension !== 'undefined'
      ? window.devToolsExtension()
      : f => f
  )(createStore)

  const store = createStoreWithMiddleware(reducers)

  return store
}

export default storeConfig
