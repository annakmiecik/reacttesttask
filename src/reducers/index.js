import { combineReducers } from 'redux'
import * as types from '../consts/actionTypes'
import bankReducers from './bank'
import editDialogReducers from './editDialog'

const rootReducer = combineReducers({
  jar_bank: bankReducers,
  dialogWindow: editDialogReducers
  //applicationPermission: ApplicationPermissionReducer,
})

export default rootReducer
