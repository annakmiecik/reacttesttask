import { combineReducers } from 'redux'
import * as types from '../consts/actionTypes'

export const initialState = {
  banks: {
    PKO: {
      name: 'PKO',
      currency: 'PLN',
      operations: [],
      saldo: 0
    },
    Raiffeissen: {
      name: 'Raiffeissen',
      currency: 'USD',
      operations: [],
      saldo: 0
    },
    WBK: {
      name: 'WBK',
      currency: 'EUR',
      operations: [],
      saldo: 0
    }
  },
  bank: {
    name: 'PKO',
    currency: 'PLN',
    operations: [],
    saldo: 0
  },
  newBank: {
    name: 'PKO',
    currency: 'PLN',
    operations: [],
    saldo: 0
  },
  targetBank: {
    name: '',
    currency: 'PLN',
    operations: [],
    saldo: 0
  }
}

export const banks = (state = initialState.banks, action) => {
  switch (action.type) {
    case types.CREATE_BANK:
      return {
        ...state,
        [action.newBank.name]: action.newBank
      }
    case types.ADD_TRANSACTION:
      let bank = { ...state[action.payload.bankName] }
      bank.saldo += action.payload.amount
      bank.operations = [
        ...bank.operations,
        {
          amount: action.payload.amount,
          date: action.payload.date
        }
      ]

      return {
        ...state,
        [action.payload.bankName]: bank
      }

    default:
      return state
  }
}

export const bank = (state = initialState.bank, action) => {
  switch (action.type) {
    case types.SET_CURRENCY:
      return {
        ...state,
        currency: action.payload
      }
    case types.SET_BANK:
      return {
        ...action.banks[action.payload]
      }
    case types.ADD_TRANSACTION:
      let bank = { ...action.banks[state.name] }
      return { ...bank }
    default:
      return state
  }
}

export const newBank = (state = initialState.newBank, action) => {
  switch (action.type) {
    case types.SET_CURRENCY_FOR_NEW_JAR:
      return {
        ...newBank,
        currency: action.payload
      }
    case types.SET_NEW_BANK:
      return {
        ...state,
        name: action.payload
      }
    default:
      return state
  }
}

export const targetBank = (state = initialState.targetBank, action) => {
  switch (action.type) {
    case types.SET_TARGET_BANK:
      return {
        ...action.banks[action.payload]
      }
    default:
      return state
  }
}

const rootReducer = (state = initialState, action) => {
  return {
    banks: banks(state.banks, {
      ...action,
      newBank: { ...{}, ...state.newBank }
    }),
    bank: bank(state.bank, { ...action, banks: { ...{}, ...state.banks } }),
    newBank: newBank(state.newBank, { ...action }),
    targetBank: targetBank(state.targetBank, {
      ...action,
      banks: { ...{}, ...state.banks }
    })
  }
}

export default rootReducer
