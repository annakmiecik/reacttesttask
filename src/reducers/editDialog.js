import { combineReducers } from 'redux'
import * as types from '../consts/actionTypes'
import { EditMode, UserRole } from '../consts/environment'

export const initialState = {
  mode: EditMode.add,
  isOpened: false,
  selected: {
    id: '',
    email: '',
    username: '',
    fullName: '',
    role: UserRole.User
  },
  runCircular: true
}

export const mode = (state = initialState.mode, action) => {
  switch (action.type) {
    case types.WINDOW_MODE:
      return action.payload
    default:
      return state
  }
}
export const isOpened = (state = initialState.isOpened, action) => {
  switch (action.type) {
    case types.WINDOW_IS_OPENED:
      return action.payload
    default:
      return state
  }
}
export const selected = (state = initialState.selected, action) => {
  switch (action.type) {
    case types.WINDOW_MODE:
      return action.payload === EditMode.add ? { ...initialState.selected } : { ...action.selected }
    case types.WINDOW_SELECTED:
      return action.payload
    default:
      return state
  }
}
export const runCircular = (state = initialState.runCircular, action) => {
  switch (action.type) {
    case types.WINDOW_RUN_CIRCULAR:
      return action.payload
    default:
      return state
  }
}

// export const dialogWindow = (state = initialState, action) => {
//   switch (action.type) {
//     case types.WINDOW_IS_OPENED:
//       return {
//         ...state,
//         isOpened: action.payload
//       }
//     case types.WINDOW_MODE:
//       return {
//         ...state,
//         mode: action.payload,
//         selected:
//           action.payload === EditMode.add ? { ...initialState.selected } : { ...state.selected }
//       }

//     case types.WINDOW_RUN_CIRCULAR:
//       return {
//         ...state,
//         runCircular: action.payload
//       }

//     case types.WINDOW_SELECTED:
//       return {
//         ...state,
//         selected: action.payload
//       }

//     default:
//       return state
//   }
// }

// const editDialogReducers = combineReducers({
//   //dialogWindow
//   mode,
//   isOpened,
//   selected,
//   runCircular
// })

const editDialogReducers = (state = initialState, action) => {
  return {
    mode: mode(state.mode, { ...action }),
    isOpened: isOpened(state.isOpened, { ...action }),
    selected: selected(state.selected, { ...action, selected: { ...{}, ...state.selected } }),
    runCircular: runCircular(state.runCircular, { ...action })
  }
}

export default editDialogReducers
