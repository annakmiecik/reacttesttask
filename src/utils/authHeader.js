export const authHeader = () => {
  // return authorization header with jwt token
  let user = JSON.parse(localStorage.getItem('user'))

  if (user && user.token) {
    return { Authorization: 'Bearer ' + user.token }
  } else {
    return {}
  }
}

export const handleResponse = (response, logout) => {
  if (response.status === 401) {
    // auto logout if 401 response returned from api
    logout()
    // location.reload(true)
    return Promise.reject('Unauthorized access!!')
  }
  return response
}
