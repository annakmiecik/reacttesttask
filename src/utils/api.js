import { getApi } from './apiCreator'
import { API_URL, Backend_Microservice } from '../consts/environment'

export const api = getApi(API_URL)
export const backend_Microservice_Api = onLogout => getApi(Backend_Microservice, onLogout)
