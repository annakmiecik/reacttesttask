import axios from 'axios'
import { X_API_KEY, CACHE_TIME } from '../consts/environment'
import { loadUserInfo } from './localstorage'
import { handleResponse } from './authHeader'

export const getApi = (api_url, onLogout) => {
  class Api {
    apiUrl = process.env.NODE_ENV === 'test' ? api_url : api_url
    userInfo = loadUserInfo()

    refreshConfig = () => {
      let newConfig = this.config()
      newConfig['Cache-Control'] = `no-cache,no-store,must-revalidate,max-age=-1,private`
      return newConfig
    }

    config = () => {
      let config = {
        baseURL: this.apiUrl,
        headers: {
          'Cache-Control': `max-age=${CACHE_TIME},public`,
          'Content-Type': 'application/json;charset=UTF-8',
          'X-API-Key': X_API_KEY,
          Authorization:
            this.userInfo && this.userInfo.accessToken
              ? `Bearer ${this.userInfo.accessToken}`
              : undefined
        },
        timeout: 200000
      }

      return config
    }

    apiRefresh = axios.create(this.refreshConfig())

    apiCache = axios.create(this.config())

    post = (url, data, headers = {}, conf = {}) => {
      const confi = this.config()
      const c = { ...confi, ...conf, headers: { ...confi.headers, ...headers } }
      if (process.env.NODE_ENV === 'production') {
        return this.apiCache
          .post(this.apiUrl + url, data, c)
          .catch(error => window.Raven.captureException(error))
      } else {
        return this.apiCache.post(this.apiUrl + url, data, c).then(
          response => {
            return response
          },
          error => {
            return handleResponse(error.response, onLogout)
          }
        )
      }
    }

    get = (url, params) => {
      var conf = this.config()
      conf = { ...conf, ...params }

      if (process.env.NODE_ENV === 'production') {
        return this.apiCache
          .get(this.apiUrl + url, conf)
          .catch(error => window.Raven.captureException(error))
      } else {
        return this.apiCache.get(this.apiUrl + url, conf).then(
          response => {
            return response
          },
          error => {
            return handleResponse(error.response, onLogout)
          }
        )
      }
    }

    put = (url, data, params) => {
      var conf = this.config()
      conf = { ...conf, ...params }

      if (process.env.NODE_ENV === 'production') {
        return this.apiCache
          .put(this.apiUrl + url, data, conf)
          .catch(error => window.Raven.captureException(error))
      } else {
        return this.apiCache.put(this.apiUrl + url, data, conf).then(
          response => {
            return response
          },
          error => {
            return handleResponse(error.response, onLogout)
          }
        )
      }
    }

    del = (url, params) => {
      var conf = this.config()
      conf = { ...conf, ...params }

      if (process.env.NODE_ENV === 'production') {
        return this.apiCache
          .delete(this.apiUrl + url, conf)
          .catch(error => window.Raven.captureException(error))
      } else {
        return this.apiCache.delete(this.apiUrl + url, conf).then(
          response => {
            return response
          },
          error => {
            return handleResponse(error.response, onLogout)
          }
        )
      }
    }

    //Builds queryString from object
    toQueryString = (data, includeAccessToken = false) => {
      return Object.keys(data)
        .map(function(key) {
          return [key, data[key]].map(encodeURIComponent).join('=')
        })
        .join('&')
    }
  }

  return new Api(api_url)
}
