const userInfoKey = 'userInfo'

export const saveUserInfo = userInfo => {
  try {
    const serializedState = JSON.stringify(userInfo)
    localStorage.setItem(userInfoKey, serializedState)
  } catch (err) {
    console.error('Can not save state to local storage. ', err)
  }
}

export const loadUserInfo = () => {
  try {
    //TODO: reconsider holding everything in localStorage, maybe in session would be better (e.g. accessToken, userAvatar)
    if (typeof localStorage !== 'undefined') {
      const serializedState = localStorage.getItem(userInfoKey)
      if (serializedState === null) {
        return undefined
      }

      const savedState = JSON.parse(serializedState)
      return savedState
    }
  } catch (err) {
    console.error('Can not read state from local storage. ', err)
  }
}
