import React from 'react'
//import logo from './logo.svg';
import './App.scss'
import './../node_modules/bootstrap/dist/css/bootstrap.css'
import Button12 from '@material-ui/core/Button'
import { Button } from './views/components/button'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import LoginProvider from './views/login/loginContext/loginProvider'
import { LoginContext } from './views/login/loginContext/loginContext'
import People from './views/people/people'
import Users from './views/users/users'
import ErrorWindow from './views/components/ErrorWindow'
import PersonDialog from './views/people/personDialog'
import DogsPictures from './views/dogsPhotos/dogsPhotos'
import CustomersChart from './views/customers/customersChart'
import Bank from './views/bank/jar'
import Login from './views/login/login'
import { PrivateRoute } from './views/login/privateRoute'
import { Router, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import storeConfig from './store/index'

const history = createHistory()
const store = storeConfig(history)

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showError: false,
      title: 'Template Error',
      message: 'Example errorMessage'
    }

    this.toogleError = this.toogleError.bind(this)
  }

  setError = (showError, title, message, onOkAction) => {
    this.setState({
      showError: showError,
      title: title,
      message: message,
      onOkAction: onOkAction
    })
  }

  toogleError() {
    this.setState({
      showError: !this.state.showError
    })
  }

  render() {
    const { showError, title, message, onOkAction } = this.state

    const menu = (
      <div className="Inline-block">
        <Button
          onClick={() => {
            history.push('/DogsPictures', null)
          }}
        >
          Map
        </Button>
        <Button
          onClick={() => {
            history.push('/', null)
          }}
        >
          People List
        </Button>
        <Button
          onClick={() => {
            history.push('/Users', null)
          }}
        >
          Users List
        </Button>
        <Button
          onClick={() => {
            history.push('/Bank', null)
          }}
        >
          Bank
        </Button>
        <Button
          onClick={() => {
            history.push('/CustomersChart', null)
          }}
        >
          CustomersChart
        </Button>
      </div>
    )

    return (
      <MuiThemeProvider>
        <Provider store={store}>
          <LoginProvider>
            <LoginContext.Consumer>
              {({ isAuthenticated, authenticate, signOut }) => (
                <Router history={history}>
                  <div>
                    <ErrorWindow
                      open={showError}
                      showError={this.setError}
                      title={title}
                      message={message}
                      onOkAction={onOkAction}
                    />
                    <div className="my-10-margin">
                      {isAuthenticated ? menu : 'user not authenticated'}
                      <div className="Float-right Inline-block">
                        <Button
                          onClick={() => {
                            history.push('/Login', null)
                          }}
                        >
                          Login
                        </Button>
                        <Button
                          onClick={() => {
                            signOut()
                            history.push('/Login', null)
                          }}
                        >
                          Logout
                        </Button>
                      </div>
                    </div>

                    <PrivateRoute
                      exact
                      path="/"
                      {...this.props}
                      showError={this.setError}
                      component={People}
                    />
                    <PrivateRoute
                      exact
                      path="/Users"
                      {...this.props}
                      showError={this.setError}
                      component={Users}
                    />
                    <PrivateRoute
                      {...this.props}
                      showError={this.setError}
                      path="/DogsPictures"
                      component={DogsPictures}
                    />
                    <PrivateRoute
                      {...this.props}
                      showError={this.setError}
                      path="/Bank"
                      component={Bank}
                    />
                    <PrivateRoute
                      path="/CustomersChart"
                      {...this.props}
                      showError={this.setError}
                      data={[12, 5, 6, 6, 9, 10]}
                      width={700}
                      height={500}
                      id={'root'}
                      component={CustomersChart}
                    />
                    <PrivateRoute
                      path="/person/:id"
                      {...this.props}
                      showError={this.setError}
                      component={PersonDialog}
                    />
                    <Route
                      path="/Login"
                      component={props => <Login {...props} showError={this.setError} />}
                    />
                    <Button12
                      variant="contained"
                      color="primary"
                      className="button"
                      onClick={this.toogleError}
                    >
                      {this.state.showError ? 'Error ON' : 'Error OFF'}
                    </Button12>
                  </div>
                </Router>
              )}
            </LoginContext.Consumer>
          </LoginProvider>
        </Provider>
      </MuiThemeProvider>
    )
  }
}

export default App
