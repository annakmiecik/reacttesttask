import React from 'react'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react'

export class Maps extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      selectedPlace: {},
      showingInfoWindow: false,
      activeMarker: {},
      apiKey: 'AIzaSyAyesbQMyKVVbBgKVi2g6VX7mop2z96jBo'
    }
  }

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    })

  onMapClicked = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  }

  render() {
    const { data } = this.props
    return (
      <Map
        google={this.props.google}
        style={{
          width: '75%',
          height: '50%',
          marginTop: 50,
          marginLeft: 'auto',
          marginRight: 'auto'
        }}
        zoom={15}
        center={
          data && data.length > 0
            ? {
                lat: data[0].latitude,
                lng: data[0].longitude
              }
            : {
                lat: 42.39,
                lng: -72.52
              }
        }
      >
        {data.map(photo => (
          <Marker
            key={photo.id}
            onClick={this.onMarkerClick}
            title={photo.title}
            name={photo.id}
            position={{ lat: photo.latitude, lng: photo.longitude }}
          />
        ))}

        <InfoWindow marker={this.state.activeMarker} visible={this.state.showingInfoWindow}>
          <div>
            <h1>{this.state.selectedPlace.title}</h1>
          </div>
        </InfoWindow>
      </Map>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyAyesbQMyKVVbBgKVi2g6VX7mop2z96jBo'
})(Maps)
