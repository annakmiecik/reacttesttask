import React from 'react'
import { getDogsPicture, getDogsPictureGeo } from '../../actions/dogsPicture'
import '../../../node_modules/bootstrap/dist/css/bootstrap.css'
import Maps from './maps'
import { PhotoRow } from './photoRow'

export default class DogsPictures extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      data: []
    }
  }

  getGeoData = photo => event => {
    getDogsPictureGeo(photo).then(geoData => {
      console.log(
        `latitude: ${geoData.photo.location.latitude},"longitude: ${
          geoData.photo.location.longitude
        }`
      )
    })
  }

  componentDidMount() {
    getDogsPicture().then(async data => {
      let fullData = data.photos.photo.map(async photo => {
        let geoData = await getDogsPictureGeo(photo.id)

        if (geoData.photo) {
          photo.latitude = geoData.photo.location.latitude
          photo.longitude = geoData.photo.location.longitude
        }

        return photo
      })

      Promise.all(fullData).then(results => {
        this.setState({ data: results })
      })
    })
  }

  render() {
    const { data } = this.state

    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">latitude</th>
              <th scope="col">longitude</th>
              <th scope="col">title</th>
              <th scope="col">id</th>
              <th scope="col">photo</th>
            </tr>
          </thead>
          <tbody>
            {data.map(photo => (
              <PhotoRow key={photo.id} photo={photo} getGeoData={this.getGeoData} />
            ))}
          </tbody>
        </table>
        <div>
          <Maps data={data} apiKey={'AIzaSyAyesbQMyKVVbBgKVi2g6VX7mop2z96jBo'} />
        </div>
      </div>
    )
  }
}
