import React from 'react'
import '../style.scss'

export const PhotoRow = props => {
  const { photo, getGeoData } = props

  return (
    <tr key={photo.id} onClick={getGeoData(photo.id)}>
      <td className="my-20-margin">{photo.latitude}</td>
      <td className="my-20-margin">{photo.longitude}</td>
      <td className="my-20-margin">{photo.title}</td>
      <td className="my-20-margin">{photo.id}</td>
      <td className="my-20-margin">
        <img
          className="imgStyle"
          src={`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${
            photo.secret
          }.jpg`}
          alt="Red dot"
        />
      </td>
    </tr>
  )
}
