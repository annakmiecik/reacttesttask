import React from 'react'
import { fakeAuth } from '../../../actions/login'
import { LoginContext } from './loginContext'

export default class LoginProvider extends React.Component {
  constructor(props) {
    super(props)

    this.authenticateAsync = (login, password) => {
      fakeAuth.authenticateAsync(login, password).then(result => {
        this.setState(state => ({
          isAuthenticated: result && result.accessToken !== undefined
        }))
      })
    }

    this.signOut = () => {
      this.setState(state => ({
        isAuthenticated: fakeAuth.signOut()
      }))
    }

    this.state = {
      isAuthenticated: true,
      authenticate: this.authenticate,
      authenticateAsync: this.authenticateAsync,
      signOut: this.signOut
    }
  }

  render() {
    return (
      <LoginContext.Provider value={{ ...this.state, authenticateAsync: this.authenticateAsync }}>
        {this.props.children}
      </LoginContext.Provider>
    )
  }
}
