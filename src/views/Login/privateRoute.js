import React from 'react'
import { Redirect, Router, Route } from 'react-router-dom'
import { LoginContext } from './loginContext/loginContext'

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <LoginContext.Consumer>
    {({ isAuthenticated, authenticate, signOut }) => (
      <Route
        {...rest}
        render={props =>
          isAuthenticated === true ? <Component {...props} {...rest} /> : <Redirect to="/login" />
        }
      />
    )}
  </LoginContext.Consumer>
)
