import React from 'react'
import { LoginContext } from './loginContext/loginContext'
//import Button_MaterialUi from '@material-ui/core/Button'
import { Button } from './../components/button'
import './login.css'
import '../style.scss'
import { directions_walk } from 'material-ui/svg-icons'
import MenuIcon from '@material-ui/icons/Face'

export default class LoginFormat extends React.Component {
  constructor(props) {
    super(props)
    this.login = React.createRef()
    this.password = React.createRef()
  }

  onClick = authenticate => {
    authenticate(this.login.current.value, this.password.current.value)
  }

  render() {
    return (
      <LoginContext.Consumer>
        {({ isAuthenticated, authenticate, authenticateAsync, signOut }) => (
          <div>
            <div className="imgcontainer">
              <MenuIcon />

              <b>{isAuthenticated ? 'isAuthenticated!' : 'is not Authenticated!'}</b>
            </div>

            <div className="container">
              <label htmlFor="uname">
                <b>Username</b>
              </label>
              <input
                type="text"
                placeholder="Enter Username"
                name="uname"
                required
                ref={this.login}
              />

              <label htmlFor="psw">
                <b>Password</b>
              </label>
              <input
                type="password"
                placeholder="Enter Password"
                name="psw"
                required
                ref={this.password}
              />
            </div>

            <div className="container gray-background">
              <Button onClick={e => this.onClick(authenticateAsync)}>Login</Button>
              <Button className="cancelbtn" onClick={signOut}>
                Cancel
              </Button>
            </div>
          </div>
        )}
      </LoginContext.Consumer>
    )
  }
}
