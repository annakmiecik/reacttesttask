import React from 'react'
//import '../../node_modules/bootstrap/dist/css/bootstrap.css'

export default class BankSelect extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.props.setBank(event.target.value)
  }

  componentDidUpdate(prevProps) {
    const { bankList } = this.props

    if (bankList && bankList.length > prevProps.bankList.length) {
      this.props.setBank(bankList[0].bankKey)
    }
  }

  render() {
    const { bankList, selected } = this.props
    return (
      <div
        className="form-group"
        data-toggle="tooltip"
        data-placement="right"
        title="Bank selector"
      >
        <label htmlFor="selector">
          <strong>Bank:</strong>
        </label>
        <div>
          <select id="selector" onChange={this.handleChange} value={selected}>
            {bankList.map(e => (
              <option key={e.bankKey} value={e.bankKey}>
                {e.bankKey}
              </option>
            ))}
          </select>
        </div>
      </div>
    )
  }
}

BankSelect.defaultProps = {
  bankList: [{ bankKey: 'PKO' }, { bankKey: 'Raiffeissen' }, { bankKey: 'WBK' }]
}
