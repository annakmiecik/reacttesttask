import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import './style.scss'

import * as table from '../../utils/table'

const rows = [
  { id: 'amount', numeric: false, disablePadding: true, label: 'Kwota' },
  { id: 'date', numeric: true, disablePadding: false, label: 'Data' },
  { id: 'saldo', numeric: true, disablePadding: false, label: 'Saldo' }
]

class HistoryTable extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'first_name',
    selected: [],
    selectedElement: {},
    page: 0,
    rowsPerPage: 5,
    data: [],
    runCircular: false
  }

  componentDidMount() {}

  handleRequestSort = (event, property) => {
    const orderBy = property
    let order = 'desc'

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc'
    }

    this.setState({ order, orderBy })
  }

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }))
      return
    }
    this.setState({ selected: [] })
  }

  handleClick = (event, selectedElement) => {
    const { selected } = this.state
    const selectedIndex = selected.indexOf(selectedElement.id)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, selectedElement.id)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }

    this.setState({ selected: newSelected })
    this.setState({ selectedElement })
  }

  handleChangePage = (event, page) => {
    this.setState({ page })
  }

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value })
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1

  render() {
    const { classes, bank } = this.props
    const { order, orderBy, selected, rowsPerPage, page } = this.state
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, bank.operations.length - page * rowsPerPage)
    let saldo = 0

    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <Table className={`${classes.table} min-width-300`} aria-labelledby="tableTitle">
            <table.EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={bank.operations.length}
              rows={rows}
            />
            <TableBody>
              {table
                .stableSort(bank.operations, table.getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.id)
                  saldo += n.amount
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox" />
                      <TableCell numeric>{n.amount}</TableCell>
                      <TableCell numeric>{n.date}</TableCell>
                      <TableCell numeric>{saldo}</TableCell>
                    </TableRow>
                  )
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={bank.operations.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page'
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page'
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    )
  }
}

HistoryTable.propTypes = {
  classes: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  bank: state.jar_bank.bank,
  banks: state.jar_bank.banks,
  newBank: state.jar_bank.newBank,
  targetBank: state.jar_bank.targetBank
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      // setBank,
      // setTargetBank,
      // setCurrency,
      // setCurrencyForNewJar,
      // createBank,
      // handleChange,
      // addTransaction,
      // addTransferBetweenJars,
    },
    dispatch
  )

export default withStyles(table.styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(HistoryTable)
)
