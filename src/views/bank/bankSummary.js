import React from 'react'

export const BankSummary = data => {
  const bank = data.bank
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">nazwa</th>
            <th scope="col">waluta</th>
            <th scope="col">saldo</th>
          </tr>
        </thead>
        <tbody>
          {
            <tr>
              <td>{bank.name}</td>
              <td>{bank.currency}</td>
              <td>{bank.saldo}</td>
            </tr>
          }
        </tbody>
      </table>
    </div>
  )
}
