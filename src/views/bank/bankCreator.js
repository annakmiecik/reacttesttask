import React from 'react'
import CurrencySelect from './currencySelector'
import BankSelect from './bankSelector'
import transactionComponentHOC from './HOC/transactionComponentHOC'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import './style.scss'

import {
  setBank,
  setTargetBank,
  setCurrency,
  setCurrencyForNewJar,
  createBank,
  handleChange,
  addTransaction
} from '../../actions/bank'

export class BankCreator extends React.Component {
  constructor(props) {
    super(props)

    this.textInput = React.createRef()
  }

  render() {
    const {
      setBank,
      setTargetBank,
      setCurrencyForNewJar,
      createBank,
      handleChange,
      addTransaction,
      bank,
      banks,
      bankKeys,
      newBank,
      targetBank,
      textInput1
    } = this.props

    const selectedBanks = Object.keys(banks)
      .filter(key => banks[key].currency === bank.currency && banks[key].name !== bank.name)
      .map(name => {
        return { bankKey: name }
      })

    return (
      <div className="containerStyle">
        <div className="borderStyle my-float-left">
          <p>
            <strong>Kreator słoików [pkt 2]</strong>
          </p>
          <form>
            Nazwa słoika:
            <br />
            <input
              type="text"
              name="bankName"
              value={newBank ? newBank.name : ''}
              onChange={handleChange}
            />
            <br />
            <br />
          </form>
          <CurrencySelect setCurrency={setCurrencyForNewJar} />
          <button type="button" className="btn btn-primary my-5-margin" onClick={createBank}>
            Utwórz słoik
          </button>
        </div>
        <div
          className="borderStyle my-float-left"
          data-toggle="tooltip"
          data-placement="right"
          title="Można dokonać przelewu tylko na słoik o zgodnej walucie"
        >
          <p>
            <strong>Przelewy [pkt 2]</strong>
          </p>
          Źródłowy słoik:
          <br />
          <BankSelect
            selected={bank.name}
            setBank={setBank}
            bankList={bankKeys}
            className="my-float-left"
          />
          Docelowy słoik:
          <br />
          <BankSelect className="my-float-left" setBank={setTargetBank} bankList={selectedBanks} />
          <div className="my-display-InlineBlock">
            <form>
              Kwota transakcji:
              <br />
              <input type="number" name="money" ref={textInput1} />
              <br />
            </form>
            <br />
            <button
              type="button"
              className="btn btn-primary my-5-margin"
              onClick={() => {
                addTransaction(textInput1.current.value, -1, bank.name)
                addTransaction(textInput1.current.value, 1, targetBank.name)
              }}
              disabled={targetBank ? false : true}
            >
              Przelej kwotę
            </button>
            <br />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    bank: state.jar_bank.bank,
    banks: state.jar_bank.banks,
    newBank: state.jar_bank.newBank,
    targetBank: state.jar_bank.targetBank
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setBank,
      setTargetBank,
      setCurrency,
      setCurrencyForNewJar,
      createBank,
      handleChange,
      addTransaction
    },
    dispatch
  )
}

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(BankCreator)

export default transactionComponentHOC(BankCreator, mapStateToProps, mapDispatchToProps)
