import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import BankSelect from './bankSelector'
import { BankSummary } from './bankSummary'
import Transaction from './transaction'
import BankCreator from './bankCreator'
import CurrencySelect from './currencySelector'
import HistoryTable from './history'
import './style.scss'
import {
  setBank,
  setTargetBank,
  setCurrency,
  setCurrencyForNewJar,
  createBank,
  handleChange
} from '../../actions/bank'

class Jar extends React.Component {
  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const { setBank, setCurrency, bank, banks } = this.props

    const bankKeys = Object.keys(banks).map(name => {
      return { bankKey: name }
    })

    return (
      <div className="JAR">
        <div className="containerStyle">
          <div className="borderStyle">
            <p>
              <strong>Explorer słoików [pkt 1]</strong>
            </p>
            <div>
              <BankSelect
                selected={bank.name}
                setBank={setBank}
                bankList={bankKeys}
                className="Float-left"
              />
              <CurrencySelect
                className="Float-left"
                selected={bank.currency}
                setCurrency={setCurrency}
              />
            </div>
            <br />
            <BankSummary bank={bank} />
            <Transaction />
          </div>
          <div>
            <br />
            <HistoryTable bank={bank} />
          </div>
        </div>
        <br />
        <BankCreator bankKeys={bankKeys} />
        <br />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    bank: state.jar_bank.bank,
    banks: state.jar_bank.banks,
    newBank: state.jar_bank.newBank,
    targetBank: state.jar_bank.targetBank
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setBank,
      setTargetBank,
      setCurrency,
      setCurrencyForNewJar,
      createBank,
      handleChange
    },
    dispatch
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Jar)
