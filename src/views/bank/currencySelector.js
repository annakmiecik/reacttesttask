import React from 'react'
//import '../../node_modules/bootstrap/dist/css/bootstrap.css'

export default class CurrencySelect extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.props.setCurrency(event.target.value)
  }

  render() {
    const { currency, selected } = this.props
    return (
      <div
        className="form-group"
        data-toggle="tooltip"
        data-placement="right"
        title="Bank selector"
      >
        <label htmlFor="selector">
          <strong>Waluta:</strong>
        </label>
        <div>
          <select id="selector" onChange={this.handleChange} value={selected}>
            {currency.map(e => (
              <option key={e.key} value={e.key}>
                {e.key}
              </option>
            ))}
          </select>
        </div>
      </div>
    )
  }
}

CurrencySelect.defaultProps = {
  currency: [{ key: 'PLN' }, { key: 'USD' }, { key: 'EUR' }]
}
