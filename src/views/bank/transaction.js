import React from 'react'
import { addTransaction } from '../../actions/bank'
import { bindActionCreators } from 'redux'
import transactionComponentHOC from './HOC/transactionComponentHOC'
import './style.scss'

export class Transaction extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { runPayInTransaction, runPayOutTransaction, textInput1 } = this.props
    return (
      <div className="my-display-InlineBlock">
        <form>
          Kwota transakcji:
          <br />
          <input type="number" name="money" id="testValue1" ref={textInput1} />
        </form>

        <button type="button" className="btn btn-primary my-5-margin" onClick={runPayInTransaction}>
          Dodaj kwotę
        </button>
        <button
          type="button"
          className="btn btn-primary my-5-margin"
          onClick={runPayOutTransaction}
        >
          Wypłać kwotę
        </button>
        <br />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    bank: state.jar_bank.bank
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addTransaction
    },
    dispatch
  )
}

export default transactionComponentHOC(Transaction, mapStateToProps, mapDispatchToProps)
