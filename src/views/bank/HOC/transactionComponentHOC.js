import React from 'react'
import { connect } from 'react-redux'
import { addTransaction } from '../../../actions/bank'

export default (WrappedComponent, mapStatetoProps, mapDispatchToProps) => {
  class TransactionHOC extends React.Component {
    constructor(props) {
      super(props)
      this.textInput1 = React.createRef()
      this.runPayInTransaction = this.runPayInTransaction.bind(this)
      this.runPayOutTransaction = this.runPayOutTransaction.bind(this)
    }

    runPayInTransaction = () => {
      const { value } = this.textInput1.current
      const { bank, addTransaction } = this.props
      addTransaction(value, 1, bank.name)
    }

    runPayOutTransaction = () => {
      const { value } = this.textInput1.current
      const { bank, addTransaction } = this.props
      addTransaction(value, -1, bank.name)
    }

    render() {
      return (
        <WrappedComponent
          {...this.props}
          textInput1={this.textInput1}
          runPayInTransaction={this.runPayInTransaction}
          runPayOutTransaction={this.runPayOutTransaction}
        />
      )
      //})
    }
  }

  return connect(
    mapStatetoProps,
    mapDispatchToProps
  )(TransactionHOC)
}
