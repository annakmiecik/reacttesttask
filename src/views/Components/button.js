import React from 'react'
import Button_MaterialUi from '@material-ui/core/Button'
import '../../../node_modules/bootstrap/dist/css/bootstrap.css'

export const Button = props => {
  return (
    <button type="button" className="btn btn-primary my-10-margin" onClick={props.onClick}>
      {props.children}
    </button>
  )
}
