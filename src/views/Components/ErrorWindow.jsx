import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import PropTypes from 'prop-types'
import '../style.scss'

class ErrorWindow extends React.Component {
  handleClickOpen = () => {
    const { title, message } = this.props
    this.props.showError(true, title, message)
  }

  handleClose = () => {
    const { title, message } = this.props
    this.props.showError(false, title, message)
  }

  render() {
    const { title, message, open, onOkAction } = this.props

    return (
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title" className="alert-dialog-title1">
            {title}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description2">
              {message ? message : 'Nothing to display'}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            {onOkAction ? (
              <div>
                <Button onClick={onOkAction} color="primary" autoFocus>
                  OK
                </Button>
                <Button onClick={this.handleClose} color="primary" autoFocus>
                  CANCEL
                </Button>
              </div>
            ) : (
              <Button onClick={this.handleClose} color="primary" autoFocus>
                OK
              </Button>
            )}
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

ErrorWindow.propTypes = {
  open: PropTypes.bool.isRequired,
  showError: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
}

export default ErrorWindow
