import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'
import CircularProgress from 'material-ui/CircularProgress'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setSelected } from '../../actions/editDialog'

import * as table from '../../utils/table'

let counter = 0
const createData = (first_name, last_name, avatar, age, background_url) => {
  counter += 1
  return { id: counter, first_name, last_name, avatar, age, background_url }
}

class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context)
  }

  state = {
    order: 'asc',
    orderBy: 'first_name',
    selected: [],
    selectedElement: {},
    page: 0,
    rowsPerPage: 5,
    data: [],
    runCircular: true
  }

  componentDidMount() {
    this.props.refreshData()
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { setSelected } = this.props
    if (this.props.data !== nextProps.data) {
      setSelected(undefined)

      this.setState({ selectedElement: undefined })
      this.setState({ selected: [] })
    }
    return true
  }

  handleRequestSort = (event, property) => {
    const orderBy = property
    let order = 'desc'

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc'
    }

    this.setState({ order, orderBy })
  }

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.data.map(n => n.id) }))
      return
    }
    this.setState({ selected: [] })
  }

  handleClick = (event, selectedElement) => {
    const { selected } = this.state
    const { setSelected } = this.props
    const selectedIndex = selected.indexOf(selectedElement.id)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, selectedElement.id)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }

    this.setState({ selected: newSelected })
    this.setState({ selectedElement: selectedElement })

    setSelected(selectedElement)
  }

  handleChangePage = (event, page) => {
    this.setState({ page })
  }

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value })
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1

  render() {
    const {
      classes,
      data,
      DialogWindow,
      handleAdd,
      handleDelete,
      refreshData,
      refreshDataWithCache,
      rows,
      runCircular,
      showError,
      tableRow,
      title
    } = this.props
    const { order, orderBy, selected, selectedElement, rowsPerPage, page } = this.state
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage)

    return runCircular ? (
      <CircularProgress className="loading" size={205} />
    ) : (
      <Paper className={classes.root}>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={refreshDataWithCache}
        >
          Load Data
        </Button>
        <table.EnhancedTableToolbar
          DialogWindow={DialogWindow}
          handleAdd={handleAdd}
          handleDelete={() => {
            handleDelete(selectedElement)
          }}
          numSelected={selected.length}
          refreshData={refreshData}
          selected={selectedElement}
          showError={showError}
          title={title}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <table.EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              rows={rows}
            />
            <TableBody>
              {table
                .stableSort(data, table.getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.id)
                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      {tableRow(n)}
                    </TableRow>
                  )
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page'
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page'
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    )
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  showError: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setSelected
    },
    dispatch
  )
}

export default connect(
  undefined,
  mapDispatchToProps
)(withStyles(table.styles)(EnhancedTable))

//export default withStyles(table.styles)(EnhancedTable)
