import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'

export default function(ComposedComponent, mapStatetoProps, actionCreators) {
  class ReduxContainer extends React.PureComponent {
    constructor(props) {
      super(props)
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  const mapDispatchToProps = dispatch => {
    return bindActionCreators(
      {
        actionCreators
      },
      dispatch
    )
  }

  return connect(
    mapStatetoProps,
    mapDispatchToProps
  )(ReduxContainer)
}
