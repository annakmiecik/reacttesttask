import React from 'react'
import * as d3 from 'd3'

class CustomersChart extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isInitialized: false }
  }
  drawChart = () => {
    if (this.state.isInitialized === false) {
      this.setState({ isInitialized: true })
      const { id, data, width, height } = this.props
      const svg = d3
        .select('body')
        .append('svg')
        .attr('width', width)
        .attr('height', height)

      svg
        .selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
        .attr('x', (d, i) => i * 100)
        .attr('y', (d, i) => height - 20 * d)
        .attr('width', 85)
        .attr('height', (d, i) => d * 20)
        .attr('fill', 'green')

      svg
        .selectAll(id)
        .data(data)
        .enter()
        .append('text')
        .text(d => d)
        .attr('x', (d, i) => i * 100)
        .attr('y', (d, i) => height - 20 * d - 3)
    }
  }
  componentDidMount() {
    this.drawChart()
  }

  componentWillUnmount() {
    d3.selectAll('svg').remove()
  }

  render() {
    const { id } = this.props
    return <div id={'#' + id} />
  }
}

export default CustomersChart
