import React from 'react'
//import '.././App.css';
import PropTypes from 'prop-types'
import './../style.scss'

export default function Person({ person }) {
  return (
    <li className="my-20-margin liStyle">
      <div className="row" className="styleBody">
        <span className="my-20-margin">{person.id}</span>
        <span className="my-20-margin">{person.first_name}</span>
        <span className="my-20-margin">{person.last_name}</span>
        <span className="my-20-margin">
          {' '}
          <img src={person.avatar} alt="Red dot" />
        </span>
      </div>
    </li>
  )
}

Person.propTypes = {
  person: PropTypes.object.isRequired
}
