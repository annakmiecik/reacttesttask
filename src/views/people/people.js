import PersonDialog from './personDialog'
import React from 'react'
import Table from '../utils/tableComponent'
import { getPeople, getPeopleNoCache } from '../../actions/people'
import TableCell from '@material-ui/core/TableCell'

let counter = 0
const createData = (first_name, last_name, avatar, age, background_url) => {
  counter += 1
  return { id: counter, first_name, last_name, avatar, age, background_url }
}

const rows = [
  { id: 'id', numeric: true, disablePadding: false, label: 'Id' },
  { id: 'first_name', numeric: false, disablePadding: true, label: 'First Name' },
  { id: 'last_name', numeric: true, disablePadding: false, label: 'Last Name' },
  { id: 'avatar', numeric: true, disablePadding: false, label: 'Avatar' }
]

const tableRow = n => [
  <TableCell key="id" numeric>
    {n.id}
  </TableCell>,
  <TableCell key="first_name" numeric>
    {n.first_name}
  </TableCell>,
  <TableCell key="last_name" numeric>
    {n.last_name}
  </TableCell>,
  <TableCell key="avatar" numeric>
    <img alt={n.id} src={n.avatar} />
  </TableCell>
]

const People = WrappedComponent => {
  return class PeopleHOC extends React.Component {
    constructor(props) {
      super(props)
      this.textInput1 = React.createRef()
      this.refreshData = this.refreshData.bind(this)
      this.refreshDataWithCache = this.refreshDataWithCache.bind(this)

      this.state = {
        data: []
      }
    }

    refreshData = () => {
      let getPeopleQuery = this.props.getPeople
      const { showError } = this.props

      if (!getPeopleQuery) {
        getPeopleQuery = getPeople
      }

      getPeopleQuery()
        .then(people => {
          if (people) {
            this.setState({ data: people })
          }
        })
        .then(
          () => {
            //to demonstrate error handling
            //throw new Error('Data In request not found')
          },
          error => {
            showError(true, 'People list error!', error.response.data.error.exception)
          }
        )
        .catch(err => showError(true, 'People list error!', err.message))
        .then(() => {
          //to demonstrate circularProgressBar working
          setTimeout(() => {
            this.setState({ runCircular: false })
          }, 2000)
        })

      //   let people = [
      //     createData('Anna', 'Anonim1', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJFSURBVDjLzZLdS9NRGMf3Fwy6jLrPKKerSAsqXzKn0xY4ZrIhbvulKwtrOCdYuWDOza02Ai96AxEEaXixTMkllDdKUjhrrmDMlMFGuLbTXO7N/b6d3+rCYFLQTQ98eTiH83yel/PwAPD+Rbz/A8D2dsupvlIRTjmdluS0XWT7WifJXu4gGUZN0q2tJHWxhSSbpGSrQRJJnKtT5AE0QEaVwMwLYH4eWF4G/H7A50Pu9StExsYQHR1FfGQEsQcPEXQ4ELzdj83T1Yl4+SkJB3iLJ4+AyUnA6QRWVgCPB5iYQE6nQ1CjQYhhEFWrsaFQ4F1jIz6ZzfB33QARlgU5QAnbo11kLSaAZsP6OvI2N4ecVIqQWIwv9fX4RrVaVYWPAwNYZdpBSo6HYweFsvwMaL97aL/TOUM/4HIB4TCwtARWLkeEBsYoJCYSIWAy4bOSAREcC0SLSkt/+4Wspp2fUammtvV6YGEB8HrB0tJJTQ0StbXYGBrCGg2OHT4aiB4QFBf8xpRcwU/KmqcyPfqfADqDRGUlUlYrnhoYdNtlbPs9CVqMFfG6XsHNgnuwdf4C/7tI7E733QI7Po6sxQKnQYk7TiWee4fhCblhf3kFzfZilHXutRVcjs2Ks/vjJ8/409puJK9roTJWw/XBAZfvfn6+ttlLsM92cIDkrhtGjpQfov2+of2uNfQJMe19jJ327P0wB/i7dT1xdV/S6lZh0N2WDx6caftzBTtFHxqbbEW462bymTnPnXedwS4QM1WcK/uXN3P3PwAfNsr5/6zP/QAAAABJRU5ErkJggg=='),
      //     createData('Donut', 'Anonim2', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //     createData('Eclair', 'Anonim3', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //     createData('Ania1', 'Anonim4', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJFSURBVDjLzZLdS9NRGMf3Fwy6jLrPKKerSAsqXzKn0xY4ZrIhbvulKwtrOCdYuWDOza02Ai96AxEEaXixTMkllDdKUjhrrmDMlMFGuLbTXO7N/b6d3+rCYFLQTQ98eTiH83yel/PwAPD+Rbz/A8D2dsupvlIRTjmdluS0XWT7WifJXu4gGUZN0q2tJHWxhSSbpGSrQRJJnKtT5AE0QEaVwMwLYH4eWF4G/H7A50Pu9StExsYQHR1FfGQEsQcPEXQ4ELzdj83T1Yl4+SkJB3iLJ4+AyUnA6QRWVgCPB5iYQE6nQ1CjQYhhEFWrsaFQ4F1jIz6ZzfB33QARlgU5QAnbo11kLSaAZsP6OvI2N4ecVIqQWIwv9fX4RrVaVYWPAwNYZdpBSo6HYweFsvwMaL97aL/TOUM/4HIB4TCwtARWLkeEBsYoJCYSIWAy4bOSAREcC0SLSkt/+4Wspp2fUammtvV6YGEB8HrB0tJJTQ0StbXYGBrCGg2OHT4aiB4QFBf8xpRcwU/KmqcyPfqfADqDRGUlUlYrnhoYdNtlbPs9CVqMFfG6XsHNgnuwdf4C/7tI7E733QI7Po6sxQKnQYk7TiWee4fhCblhf3kFzfZilHXutRVcjs2Ks/vjJ8/409puJK9roTJWw/XBAZfvfn6+ttlLsM92cIDkrhtGjpQfov2+of2uNfQJMe19jJ327P0wB/i7dT1xdV/S6lZh0N2WDx6caftzBTtFHxqbbEW462bymTnPnXedwS4QM1WcK/uXN3P3PwAfNsr5/6zP/QAAAABJRU5ErkJggg=='),
      //     createData('Donut1', 'Anonim5', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //     createData('Eclair1', 'Anonim6', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //   ]

      //  this.setState({data : people})
      //  setTimeout(() => { this.setState({runCircular : false}) }, 2000);
    }

    refreshDataWithCache = () => {
      const { showError } = this.props
      getPeopleNoCache()
        .then(people => {
          if (people) {
            this.setState({ data: people })
          }
        })
        .then(
          () => {
            //to demonstrate error handling
            //throw new Error('Data In request not found')
          },
          error => {
            showError(true, 'People list error!', error.response.data.error.exception)
          }
        )
        .catch(err => showError(true, 'People list error!', err.message))
        .then(() => {
          //to demonstrate circularProgressBar working
          setTimeout(() => {
            this.setState({ runCircular: false })
          }, 2000)
        })

      //   let people = [
      //     createData('Anna', 'Anonim1', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJFSURBVDjLzZLdS9NRGMf3Fwy6jLrPKKerSAsqXzKn0xY4ZrIhbvulKwtrOCdYuWDOza02Ai96AxEEaXixTMkllDdKUjhrrmDMlMFGuLbTXO7N/b6d3+rCYFLQTQ98eTiH83yel/PwAPD+Rbz/A8D2dsupvlIRTjmdluS0XWT7WifJXu4gGUZN0q2tJHWxhSSbpGSrQRJJnKtT5AE0QEaVwMwLYH4eWF4G/H7A50Pu9StExsYQHR1FfGQEsQcPEXQ4ELzdj83T1Yl4+SkJB3iLJ4+AyUnA6QRWVgCPB5iYQE6nQ1CjQYhhEFWrsaFQ4F1jIz6ZzfB33QARlgU5QAnbo11kLSaAZsP6OvI2N4ecVIqQWIwv9fX4RrVaVYWPAwNYZdpBSo6HYweFsvwMaL97aL/TOUM/4HIB4TCwtARWLkeEBsYoJCYSIWAy4bOSAREcC0SLSkt/+4Wspp2fUammtvV6YGEB8HrB0tJJTQ0StbXYGBrCGg2OHT4aiB4QFBf8xpRcwU/KmqcyPfqfADqDRGUlUlYrnhoYdNtlbPs9CVqMFfG6XsHNgnuwdf4C/7tI7E733QI7Po6sxQKnQYk7TiWee4fhCblhf3kFzfZilHXutRVcjs2Ks/vjJ8/409puJK9roTJWw/XBAZfvfn6+ttlLsM92cIDkrhtGjpQfov2+of2uNfQJMe19jJ327P0wB/i7dT1xdV/S6lZh0N2WDx6caftzBTtFHxqbbEW462bymTnPnXedwS4QM1WcK/uXN3P3PwAfNsr5/6zP/QAAAABJRU5ErkJggg=='),
      //     createData('Donut', 'Anonim2', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //     createData('Eclair', 'Anonim3', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //     createData('Ania1', 'Anonim4', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJFSURBVDjLzZLdS9NRGMf3Fwy6jLrPKKerSAsqXzKn0xY4ZrIhbvulKwtrOCdYuWDOza02Ai96AxEEaXixTMkllDdKUjhrrmDMlMFGuLbTXO7N/b6d3+rCYFLQTQ98eTiH83yel/PwAPD+Rbz/A8D2dsupvlIRTjmdluS0XWT7WifJXu4gGUZN0q2tJHWxhSSbpGSrQRJJnKtT5AE0QEaVwMwLYH4eWF4G/H7A50Pu9StExsYQHR1FfGQEsQcPEXQ4ELzdj83T1Yl4+SkJB3iLJ4+AyUnA6QRWVgCPB5iYQE6nQ1CjQYhhEFWrsaFQ4F1jIz6ZzfB33QARlgU5QAnbo11kLSaAZsP6OvI2N4ecVIqQWIwv9fX4RrVaVYWPAwNYZdpBSo6HYweFsvwMaL97aL/TOUM/4HIB4TCwtARWLkeEBsYoJCYSIWAy4bOSAREcC0SLSkt/+4Wspp2fUammtvV6YGEB8HrB0tJJTQ0StbXYGBrCGg2OHT4aiB4QFBf8xpRcwU/KmqcyPfqfADqDRGUlUlYrnhoYdNtlbPs9CVqMFfG6XsHNgnuwdf4C/7tI7E733QI7Po6sxQKnQYk7TiWee4fhCblhf3kFzfZilHXutRVcjs2Ks/vjJ8/409puJK9roTJWw/XBAZfvfn6+ttlLsM92cIDkrhtGjpQfov2+of2uNfQJMe19jJ327P0wB/i7dT1xdV/S6lZh0N2WDx6caftzBTtFHxqbbEW462bymTnPnXedwS4QM1WcK/uXN3P3PwAfNsr5/6zP/QAAAABJRU5ErkJggg=='),
      //     createData('Donut1', 'Anonim5', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //     createData('Eclair1', 'Anonim6', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC'),
      //   ]

      //  this.setState({data : people})
      //  setTimeout(() => { this.setState({runCircular : false}) }, 2000);
    }

    render() {
      const { data, runCircular } = this.state

      return (
        <WrappedComponent
          {...this.props}
          data={data}
          DialogWindow={PersonDialog}
          runCircular={runCircular}
          textInput1={this.textInput1}
          refreshData={this.refreshData}
          refreshDataWithCache={this.refreshDataWithCache}
          rows={rows}
          tableRow={tableRow}
          title="Persons list"
        />
      )
    }
  }
}

export default People(Table)
