import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import EditIcon from '@material-ui/icons/Edit'
import IconButton from '@material-ui/core/IconButton'
import { getPerson } from '../../actions/people'
import CircularProgress from 'material-ui/CircularProgress'
import PropTypes from 'prop-types'
import './../style.scss'
import Tooltip from '@material-ui/core/Tooltip'

class PersonDialog extends React.Component {
  state = {
    open: false,
    runCircular: true
  }

  handleClickOpen = () => {
    this.setState({ open: true })
    const { selected, showError, match } = this.props

    let matchId = match && match.params && match.params.id ? match.params.id : null

    let id = selected && selected.id ? selected.id : matchId

    if (id) {
      getPerson(id)
        .then(
          person => {
            if (person) {
              this.setState({ selected: person })
            }
          },
          error => {
            showError(true, 'Person data error!', error.response.data.error.exception)
            return null
          }
        )
        .then(() => {
          //to demonstrate error handling
          //throw new Error('Data In request not found')
        })
        .catch(err => showError(true, 'Person data error!', err.message))
        .then(() => {
          //to demonstrate circularProgressBar working
          setTimeout(() => {
            this.setState({ runCircular: false })
          }, 2000)
        })
    }
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  componentWillMount() {
    const { match } = this.props
    if (match && match.params && match.params.id) {
      this.handleClickOpen()
    }
  }

  render() {
    const { selected, runCircular } = this.state
    let dialogStyle = {
      // width : '20%',
      // height : '80%',
    }

    if (selected && selected.background_url) {
      dialogStyle['backgroundImage'] = 'url("' + selected.background_url + '")'
    }

    return (
      <div className="positionStyle">
        <Tooltip title="Edit">
          <IconButton aria-label="Edit" onClick={this.handleClickOpen}>
            <EditIcon />
          </IconButton>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{'Person details'}</DialogTitle>
          <DialogContent style={dialogStyle}>
            {runCircular ? (
              <CircularProgress className="loading" />
            ) : (
              <DialogContentText id="alert-dialog-description2">
                {selected ? (
                  <form>
                    Id:
                    <br />
                    <input type="text" name="id" value={selected.id} />
                    <br />
                    First name:
                    <br />
                    <input type="text" name="firstname" value={selected.first_name} />
                    <br />
                    Last name:
                    <br />
                    <input type="text" name="lastname" value={selected.last_name} />
                    <br />
                    Age:
                    <br />
                    <input type="text" name="age" value={selected.age} />
                    <br />
                    Avatar:
                    <br />
                    <img alt={selected.id} src={selected.avatar} />
                    <br />
                    Background_url:
                    <br />
                    <input type="text" name="background_url" value={selected.background_url} />
                    <br />
                    <br />
                  </form>
                ) : (
                  <p>Nothing to display</p>
                )}
              </DialogContentText>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

PersonDialog.propTypes = {
  showError: PropTypes.func.isRequired
}

export default PersonDialog
