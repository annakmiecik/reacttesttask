import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { EditMode } from '../../consts/environment'
import EditIcon from '@material-ui/icons/Edit'
import IconButton from '@material-ui/core/IconButton'
import { add, update } from '../../actions/users'
import CircularProgress from 'material-ui/CircularProgress'
import { LoginContext } from '../login/loginContext/loginContext'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import './../style.scss'
import Tooltip from '@material-ui/core/Tooltip'
import { setIsOpened, setSelected, setRunCircular, setMode } from '../../actions/editDialog'

class UserDialog extends React.Component {
  constructor(props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  handleInputChange(event) {
    const { selected, setIsOpened, setSelected, runCircular, setMode } = this.props
    //    { mode, isOpened, selected, runCircular}
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    setSelected({ ...selected, [name]: value })
  }

  handleClickOpen = () => {
    this.setState({ open: true })
    const {
      selected,
      setIsOpened,
      setMode,
      setSelected,
      setRunCircular,
      showError,
      match
    } = this.props

    let matchId = match && match.params && match.params.id ? match.params.id : null

    let id = selected && selected.id ? selected.id : matchId

    setMode(EditMode.edit)
    setIsOpened(true)

    setTimeout(() => {
      if (selected) {
        setRunCircular(false)
        //setSelected(selected)
      }
    }, 2000)
  }

  handleClose = () => {
    //dispatch actions
    const { setIsOpened } = this.props
    setIsOpened(false)
    //this.setState({ open: false })
  }

  handleSave = () => {
    const { refreshData, mode, selected } = this.props
    const { signOut } = this.context

    let saveOperation = update
    if (mode === EditMode.add) {
      saveOperation = add
    }

    saveOperation(selected, signOut)
      .then(() => refreshData(signOut))
      .then(() => {
        this.handleClose()
      })
  }

  componentWillMount() {
    const { match } = this.props
    if (match && match.params && match.params.id) {
      this.handleClickOpen()
    }
  }

  static contextType = LoginContext

  render() {
    const { isOpened, numSelected, selected, runCircular } = this.props
    let dialogStyle = {
      // width : '20%',
      // height : '80%',
    }

    // if (selected && selected.background_url) {
    //   dialogStyle['backgroundImage'] = 'url("' + selected.background_url + '")'
    // }

    let editControl =
      numSelected === 1 ? (
        <Tooltip title="Edit">
          <IconButton aria-label="Edit" onClick={this.handleClickOpen}>
            <EditIcon />
          </IconButton>
        </Tooltip>
      ) : null

    return (
      <div className="positionStyle">
        {editControl}
        <Dialog
          open={isOpened}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{'Person details'}</DialogTitle>
          <DialogContent style={dialogStyle}>
            {runCircular ? (
              <CircularProgress className="loading" />
            ) : (
              <div id="alert-dialog-description2">
                {selected ? (
                  <form>
                    Id:
                    <br />
                    <input type="text" name="id" value={selected.id} readOnly />
                    <br />
                    E-mail:
                    <br />
                    <input
                      type="text"
                      name="email"
                      value={selected.email}
                      onChange={this.handleInputChange}
                    />
                    <br />
                    Role:
                    <br />
                    <input
                      type="text"
                      name="role"
                      value={selected.role}
                      onChange={this.handleInputChange}
                    />
                    <br />
                    User Name:
                    <br />
                    <input
                      type="text"
                      name="username"
                      value={selected.username}
                      onChange={this.handleInputChange}
                    />
                    <br />
                    Full Name:
                    <br />
                    <input
                      id="fullName"
                      type="text"
                      name="fullName"
                      value={selected.fullName}
                      onChange={this.handleInputChange}
                    />
                    <br />
                    <br />
                  </form>
                ) : (
                  <p>Nothing to display</p>
                )}
              </div>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleSave} color="primary" autoFocus>
              OK
            </Button>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              CANCEL
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

UserDialog.propTypes = {
  showError: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    mode: state.dialogWindow.mode,
    isOpened: state.dialogWindow.isOpened,
    selected: state.dialogWindow.selected,
    runCircular: state.dialogWindow.runCircular
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setIsOpened,
      setSelected,
      setRunCircular,
      setMode
    },
    dispatch
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDialog)

//export default UserDialog
