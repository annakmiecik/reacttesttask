import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { EditMode } from '../../consts/environment'
import { LoginContext } from '../login/loginContext/loginContext'
import Table from '../utils/tableComponent'
import { add, del, getAll } from '../../actions/users'
import TableCell from '@material-ui/core/TableCell'
import UserDialog from './userDialog'
import { setIsOpened, setSelected, setRunCircular, setMode } from '../../actions/editDialog'

let counter = 0

const rows = [
  { id: 'id', numeric: true, disablePadding: false, label: 'Id' },
  { id: 'email', numeric: false, disablePadding: true, label: 'E-mail' },
  { id: 'role', numeric: false, disablePadding: false, label: 'Role' },
  { id: 'username', numeric: false, disablePadding: false, label: 'User Name' },
  { id: 'fullName', numeric: false, disablePadding: false, label: 'Full Name' }
]

const tableRow = n => [
  <TableCell key="id" numeric>
    {n.id}
  </TableCell>,
  <TableCell key="email" numeric>
    {n.email}
  </TableCell>,
  <TableCell key="role" numeric>
    {n.role}
  </TableCell>,
  <TableCell key="username" numeric>
    {n.username}
  </TableCell>,
  <TableCell key="fullName" numeric>
    {n.fullName}
  </TableCell>
]

const Users = WrappedComponent => {
  return class UsersHOC extends React.Component {
    constructor(props) {
      super(props)
      this.textInput1 = React.createRef()
      this.refreshData = this.refreshData.bind(this)

      this.state = {
        data: []
      }
    }

    refreshData = () => {
      const { showError } = this.props
      const { signOut } = this.context
      const getDataQuery = getAll

      getDataQuery(signOut)
        .then(users => {
          if (users) {
            this.setState({ data: users })
          }
        })
        // .then(
        //   () => {
        //     //to demonstrate error handling
        //     //throw new Error('Data In request not found')
        //   },
        //   error => {
        //     showError(true, 'Users list error!', error.response.data.error.exception)
        //   }
        // )
        .catch(err => showError(true, 'Users list error!', err.message))
        .then(() => {
          //to demonstrate circularProgressBar working
          setTimeout(() => {
            this.setState({ runCircular: false })
          }, 2000)
        })

      //  this.setState({data : people})
      //  setTimeout(() => { this.setState({runCircular : false}) }, 2000);
    }

    handleAdd = () => {
      const { setIsOpened, setSelected, setRunCircular, setMode, selected } = this.props

      setMode(EditMode.add)
      //setSelected()
      setIsOpened(true)
      setRunCircular(false)
    }

    handleDelete = selected => {
      const { showError } = this.props
      const { signOut } = this.context

      if (selected && selected.id) {
        showError(
          true,
          'Users will be deleted!',
          `Do You really want do delete User: ${selected.fullName}`,
          () => {
            del(selected.id, signOut)
              .then(() => this.refreshData())
              .then(() => showError(false))
          }
        )
      }
    }

    static contextType = LoginContext

    render() {
      const { data, runCircular } = this.state

      return (
        <WrappedComponent
          {...this.props}
          data={data}
          DialogWindow={UserDialog}
          handleAdd={this.handleAdd}
          handleDelete={this.handleDelete}
          runCircular={runCircular}
          textInput1={this.textInput1}
          refreshData={this.refreshData}
          rows={rows}
          tableRow={tableRow}
          title="Users list"
        />
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    mode: state.dialogWindow.mode,
    isOpened: state.dialogWindow.isOpened,
    selected: state.dialogWindow.selected,
    runCircular: state.dialogWindow.runCircular
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setIsOpened,
      setSelected,
      setRunCircular,
      setMode
    },
    dispatch
  )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Users(Table))

//export default Users(Table)
