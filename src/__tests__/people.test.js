import React from 'react'
import CircularProgress from 'material-ui/CircularProgress'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { MemoryRouter } from 'react-router-dom'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'
import PropTypes from 'prop-types'
import { EnhancedTable, EnhancedTableHead, TableBody } from '../views/people'
import PersonDialog from '../views/personDialog'
import People from '../views/people'
import * as testHelper from '../utils/tests/helpers'
import App from '../App'
import { getPeople } from '../actions/people'

const mountWithStore = (component, store) => {
  const muiTheme = getMuiTheme(darkBaseTheme)
  const context = {
    store,
    muiTheme
  }

  const childContextTypes = {
    muiTheme: PropTypes.object,
    store: PropTypes.object
  }

  return global.shallow(component, { context, childContextTypes }).dive({ childContextTypes })
}

describe('App Component', () => {
  it('renders the people wrapper', () => {
    const wrapper = global.mount(<App />)
    global.expect(wrapper.find(People)).to.have.length(1)
  })
})

describe('People Component', () => {
  it('renders the People list wrapper', () => {
    const showError = t => {}
    const wrapper = global.shallow(<People showError={showError} />)
    // console.log(wrapper.children());
    // global.expect(wrapper.find(People)).to.have.length(1);
  })
})

// ///////////click tests
it('as a user I should see loading indicator when data is loading', () => {
  const wrapperApp = global.mount(<App />)
  const wrapper = wrapperApp.find(People).first()
  global.expect(wrapper.find(CircularProgress)).to.have.length(1)
})

// it('display peopleList and click by user', () => {
//     let showError = (t)=>{}

//     const wrapperApp = global.mount(<App />);
//     const wrapper = wrapperApp.find(People).first();
//     console.log(wrapper.debug())

//     const etWrapper = wrapper.find(CircularProgress).first()
//     console.log(etWrapper.debug())

//     global.expect(wrapper.find(EnhancedTable)).to.have.length(1);

//      expect(wrapper.find('tr')).to.have.lengthOf(5);
//      console.log(wrapper.find('tr').debug())
//      wrapper.find('tr').at(0).simulate('click');
//      console.log(wrapper.find('[aria-label="Edit"]').debug())
//      wrapper.find('[aria-label="Edit"]').simulate('click');
//      global.expect(wrapper.find(PersonDialog)).to.have.length(1);
//     // expect(wrapper.state().counter).to.equal(1);
//   });

it('openByUrl', () => {
  const wrapperApp = global.mount(
    <MemoryRouter initialEntries={['/']}>
      <App />
    </MemoryRouter>
  )
  const wrapper = wrapperApp.find(People).first()
  global.expect(wrapper.find(CircularProgress)).to.have.length(1)
})

// http://localhost:3000//person/1
it('as a user I should be able to access person details by entering URL', () => {
  const wrapperApp = global.mount(
    <MemoryRouter initialEntries={['/person/1']}>
      <App />
    </MemoryRouter>
  )
})

describe('People Component async without data', () => {
  it('appear circular, data is loaded, circular is gone, empty table displayed', done => {
    const showError = t => {}

    const wrapperApp = mountWithStore(<People showError={showError} />)

    // console.log(wrapperApp.debug());
    // We have CircularProgress appeared
    global.expect(wrapperApp.find('.loading')).to.have.length(1)

    // console.log("People List state: ")
    wrapperApp.setState({
      runCircular: false
    })
    wrapperApp.instance().forceUpdate()
    wrapperApp.update()

    // CircularProgress is gone, we have empty table
    global.expect(wrapperApp.find('.EnhancedTable-root-15')).to.have.length(1)
    global.expect(wrapperApp.find('.loading')).to.have.length(0)

    done()
  })
})

describe('People Component async test with mocked data', () => {
  it('as a user I can sort users by last name     ====>   appear circular, data is loaded, circular is gone, empty table displayed, table is filled with data', done => {
    const data = [
      {
        id: 6,
        first_name: 'Giustina',
        last_name: 'Edscer',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADPSURBVDjL3VIxCsJAEJyTaCmIYCFWfsMuviNPsAzkB/7E3gdob2+V1kKwsUmOmLu9c+8wASVocalcWGY5dmdmlxPWWoTEAIERNUWWZa0VIkJd16iqClJKn2VZ+iyKwmOe58L1itAVWgdpmtpfqs37q6dnB0mS2C+qxKonxtUnQT8OtvvbYTEdxl0NRBbaGChGpcBIeGjgfJHH3Wa59gRueDZut4ExFmTcIKcWjG4Q0JHhOvKk88kofrvB9a46lRRPKybQ2nii3m8Q/JX/gOAJ1Om5dnjvy8QAAAAASUVORK5CYII='
      },
      {
        id: 2,
        first_name: 'Gabriele',
        last_name: 'Palffrey',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAMxSURBVDjLTZNdaBxlFIafmd3tTmYnJU26TYgNbZoQ7GIMBH9iNQj1QimIFCL+EVEQQa+VXBQKBfVGIlL0QtDeBFEwFSlN0EaqgkaQ2tYG89OYxiaN2d1ssplNZmfONzv7ebEx9sC5fB/e874cQ2uNN26YwGngJCBAf+qEFu4ab9xoBi4BAfBy6oSeAzCKX5MA3k20vfTinvbX7vEXP8vPrrrzX9nnK0D3jn5qoPycmWlrycSTTYbMvvMXMNBwUi8buS84ZR0ZfNM69HxraXF08/byXPz71guOk4yoS1QB8EMT5Xsczw6UDx99hlhU8sozw8tAn6kE07AOWcX50c35hTlnsu1Lp71e0ej7yK0NvPkNnJJHS/0erjYP26uLk1asqa9R1x11lHDEWPiE/tCwP103Ow/+0vGN3WbHSZYj7u9spGGvicZgw434bXaNsC5GauM893qjktienk7q0guG1pq3z118P9Zw+K2OZocG0Tx7vJ2i61LwfNpaWwCoaPh8fIGqo8nmVyl48fc+fuWRUybAaqX1waqG6pbivq4myhVNIpFg4rtvKbkuoQiXJn4g07UfN+/jm/twVfJRgDiA+F53RRm4UqWpqQ6JNCnb5s7SEhOXf2Lqj+s88eRT7Dtgs1bw0Q0JxPe6dwHK9/DKSfyYgUS13vLuFinHYXstR+fBVm7duEZ2ZYW0FIndWWd/GDaeOXP5d3MHMFXaKmPEqyxmy0SGCaZF7wN9xEyDTCZDV1cXxWwWQpdMJkNPTw8i0mvunPBjyXXZCIRfr+VIJCFlJXis/xhhGFKpVAjDEK/sI0pQSiEiiAg1B4F30V3/Z3pmaZ1cMce50Tlur2xiSIhSandFBN8PdsUiUsvg57NvXHn49eGzHanND6YWK/b6lpDN5YlVTQ7cJQ5EiELRSilDa/0/AODDV4/drK+vt/OFYnR69GZsxrAxYxZPp2uAMAwxq4qqjgyR2p8ppWqAsbGxNHAhnU7jbbvudvHvj6Kqfhzolr1mo4gQBAFBEGittRGGIVprlFIYWmtGRkYc4ArQDvQODg7++Z+zoaGhG0qp7p0wJy3LeqhQKMR3gr36LzexzEk6GOJQAAAAAElFTkSuQmCC'
      },
      {
        id: 3,
        first_name: 'Kristofor',
        last_name: 'Freyn',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIQSURBVDjLjZPbbtNAFEVLvoNK/ENRv6W/guChbzwXJRJBDRIgVQglUjClddxUpCEF00RJE8fgXKBtZGzHud+vzWbOgE2gRdTSfrBn9jr7nPGsAFghsec20xrT+n90h8nj+pYAa+PxuD2bzS7n8zmuE1uH1+vdWoYsA9bJ3O/3MRgMXHU6HbRaLVSrVQ4xTRM+n8+FOOZbjHy/VCohnU4jmUwim81C13X0ej20223Yts0Bw+EQVMTv9/+E5HI5TyaTeZhKpRbNZpNvJFOj0YAkScjn8zxFrVa70hKfCTNvkHk0GoGkqiq63S5YO1yCIKBcLnNIvV7nBQzD+A1gZpGqKYrCo1JE0mQy4QDLshCLxfg8CEzzoP0uQJblCg2Geh/2WwiFQjw6GS4qOooXFl69OeQnQGBqj0AuIJF4XzHKu9BST9EzJeztBxGPx3FudZA4PUNKM7ATPsB0OuWpnIQugMUTbbMAw/yK/PckTvWPOLeLMCwbn5QznHzWIURivB0CkCiNC4hGoxu7EWGRN5I4+XaEY+0AcTUCtaigatexvfMaXwolnoBE5j8Aoih6gnsvHz1/+3hxXIhCLr3Dh8IhZC2GQCAANiNe1QE4cgHOj/Rg897m/pGAF8I2noWfICwFoRU09zj/1hUAvbCPi3/dg2t06QJ+Qe6yqANauImZ7e3x27sEWCXIDa6zI7r6qz8AeSLtQ3VwWP8AAAAASUVORK5CYII='
      },
      {
        id: 4,
        first_name: 'Kittie',
        last_name: 'Snar',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIhSURBVDjLY/j//z8DJZgsTV+9fAu+uHo8+GzvXECWAV+c3R//mTn9/ydLu4eka3ZyY/ts63T3k4Xt+4/GlqS74JONY+9Hc5tdH4wsmAmGgWv9xQKX2nMPnapOF4A1WzsEfjSzefLB0FwUHoi/szPX/05P/f0rOWk9ugHONWefzNl44X/B/L3/o7LXnn1h4fitN6i22Tx7W5tpxqYHxmnrChh+p6X+/rd10/+fsbF/f0REmiE0n7F3rDz5wb7s6Bu3gt3Vz80db69zTd1mlr11tUnGxt89Cw/8N0ha9YDhZ2LC+p8xMb9/hEdc+h4Ucu+br//JFXFNi5zKjz20KztiDzIMGFgzP+iZboQZbpSypsAgaeUjvfilqIEI9C9bf8rk3Wd8kz59sHV+BQysa8DA+vNe1+RreV94S96UiE9pff7/I1scPnlW6NWgBCLQvxKOVaeO2ZcfW2pbcogTGFgGwMD6+2/alP+rYhz+Na5O/L/lytT/F57t+t+/O+t/eL/uf/NsyR4G17oLBUD/Pgf69w3Qv6XILnqvbbT+nZre74RWlz8bL0/4v/HapP8g0LMn9X//nnSQAd8ZnKrPPJi85uJ/oH9f4opOn2rD/9uuzPmPDDZdmgoy4D+DQ8XxArvSww9sivYX4DLAMkf6e/eupP/tuxLAmtt3JiBcQEzqAypsCe7R+N+7KwVsM4gG8cFhQGwSBiruAOJPIGdD6Q6QOAAJO6JfeUJqowAAAABJRU5ErkJggg=='
      },
      {
        id: 5,
        first_name: 'Jacintha',
        last_name: 'Casarino',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADPSURBVCjPdZFNCsIwEEZHPYdSz1DaHsMzuPM6RRcewSO4caPQ3sBDKCK02p+08DmZtGkKlQ+GhHm8MBmiFQUU2ng0B7khClTdQqdBiX1Ma1qMgbDlxh0XnJHiit2JNq5HgAo3KEx7BFAM/PMI0CDB2KNvh1gjHZBi8OR448GnAkeNDEDvKZDh2Xl4cBcwtcKXkZdYLJBYwCCFPDRpMEjNyKcDPC4RbXuPiWKkNABPOuNhItegz0pGFkD+y3p0s48DDB43dU7+eLWes3gdn5Y/LD9Y6skuWXcAAAAASUVORK5CYII='
      },
      {
        id: 6,
        first_name: 'Charita',
        last_name: 'Irvine',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJtSURBVDjLjZPLa1NREIe/m1dr1aY22tb4qEkEX4hYaKG+7UIFS5auBOlG6bbgHyC4EHEhKCjiVhBcaVWsBSVgikLF2iqxPqI1llrTxiba9N6Tc+64CAaDFRw4DDNwvt/MOTOICP9zpjo7ZbG8h/+0/p6eRfOWiFSCVColxhi01vz2Wmuy2SzJZJJwOEwsFiMej1u/7/j+pJVKJaLRKAB/gkWErq4uRISBgYGqCqoAWmsAHo+XOzMCxgVXXIyBI1s1juP8G6CUQkRoa/m+6EOq7Cgd3vu8v5OR+ZkElj15YlHA0y8rcMVFu+UKtIFmfY+dnhe0bD9OMLKTuY+bePPw+vm/AADtq2eqlK1CgqB/gqX1u8l9SBGwFMvrW1gWWh+sAjiOg4iQ+NSIEUG7ENJD7A+nCUYO40zdJFBnMTHyhpKtZu2Fn7uqAHkzh4iwZ122rF54RqNvjGAsjj15FU9A41/WSm1xbLr403ekvW/wbWWQTj/t3fC6YRgRwXVd7t84w1L9kuDGOPbkFTz+EqqwgczjZ6gfxe62vsGRql9wXTfi8XtJ5ceo+/yOg+2raNp2FPX1Ot6AYOdb+fbkOdeCIT54fev7YRgoj3Jf8lQ9xnoU8q9kKHGZgJ3GsI6psUu42Mzn1jA99IoLfi8NzTG0MhcOXezaXgEYbXodpeRddpzW6Rxb95ykJvOEt7eTjAzkeNGf5IG7mYJaQq4wg9Y6bLR7ttKCuVV7DsACxtU06vVdIjv2ks/Okh5OMVp3gMLXIo1WE7OZ76xVrTU1qjb+1zIBHNvXuNAWbfZ1bIkgfmdo4Vu2p/vcaPpfW/oLvSN/oHdKKLQAAAAASUVORK5CYII='
      },
      {
        id: 1,
        first_name: 'Kittie',
        last_name: 'Znar',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIhSURBVDjLY/j//z8DJZgsTV+9fAu+uHo8+GzvXECWAV+c3R//mTn9/ydLu4eka3ZyY/ts63T3k4Xt+4/GlqS74JONY+9Hc5tdH4wsmAmGgWv9xQKX2nMPnapOF4A1WzsEfjSzefLB0FwUHoi/szPX/05P/f0rOWk9ugHONWefzNl44X/B/L3/o7LXnn1h4fitN6i22Tx7W5tpxqYHxmnrChh+p6X+/rd10/+fsbF/f0REmiE0n7F3rDz5wb7s6Bu3gt3Vz80db69zTd1mlr11tUnGxt89Cw/8N0ha9YDhZ2LC+p8xMb9/hEdc+h4Ucu+br//JFXFNi5zKjz20KztiDzIMGFgzP+iZboQZbpSypsAgaeUjvfilqIEI9C9bf8rk3Wd8kz59sHV+BQysa8DA+vNe1+RreV94S96UiE9pff7/I1scPnlW6NWgBCLQvxKOVaeO2ZcfW2pbcogTGFgGwMD6+2/alP+rYhz+Na5O/L/lytT/F57t+t+/O+t/eL/uf/NsyR4G17oLBUD/Pgf69w3Qv6XILnqvbbT+nZre74RWlz8bL0/4v/HapP8g0LMn9X//nnSQAd8ZnKrPPJi85uJ/oH9f4opOn2rD/9uuzPmPDDZdmgoy4D+DQ8XxArvSww9sivYX4DLAMkf6e/eupP/tuxLAmtt3JiBcQEzqAypsCe7R+N+7KwVsM4gG8cFhQGwSBiruAOJPIGdD6Q6QOAAJO6JfeUJqowAAAABJRU5ErkJggg=='
      }
    ]

    const getPeopleQuery = () => Promise.resolve(data)
    let promise

    const loadData = () => {
      promise = Promise.resolve().then(getPeopleQuery)
      return promise
    }

    const showError = t => {}

    const wrapperApp = mountWithStore(<People showError={showError} getPeople={loadData} />)

    // We have CircularProgress appeared
    global.expect(wrapperApp.find('.loading')).to.have.length(1)

    // We set off CircularProgress, because demonstration app waits 2sec`s after data is loaded,
    // before it shows data
    wrapperApp.setState({
      runCircular: false
    })
    wrapperApp.instance().forceUpdate()
    wrapperApp.update()

    // CircularProgress is gone, we have empty table
    global.expect(wrapperApp.find('.EnhancedTable-root-15')).to.have.length(1)
    global.expect(wrapperApp.find('.loading')).to.have.length(0)
    // console.log(wrapperApp.debug());

    promise.then(() => {
      const headerWrapper = wrapperApp.find('EnhancedTableHead').dive()

      const tableSortByIdLabel = headerWrapper
        .find('[active=false]')
        .at(0)
        .dive()

      // Issue solution: https://github.com/airbnb/enzyme/issues/1081
      tableSortByIdLabel.simulate('click')
      tableSortByIdLabel.simulate('click')

      // console.log(headerWrapper.debug());
      // console.log(wrapperApp.debug());

      global.expect(wrapperApp.find('[alt=1]')).to.have.length(1)

      const tableSortByLastNameLabel = headerWrapper
        .find('[active=false]')
        .at(1)
        .dive()

      tableSortByLastNameLabel.simulate('click')
      tableSortByLastNameLabel.simulate('click')

      // After sorting by last_name there is no record on first page
      global.expect(wrapperApp.find('[alt=1]')).to.have.length(0)

      // wrapperApp.instance().forceUpdate()
      // wrapperApp.update();
      // console.log(wrapperApp.debug());

      done()
    })
  })
})

describe('People Component async test E2E', () => {
  it('as a user I can display list of people      ====>   appear circular, data is loaded, circular is gone, empty table displayed, table is filled with data from API', done => {
    getPeople().then(people => {
      const getPeopleQuery = () => Promise.resolve(people)

      let promise

      const loadData = () => {
        promise = Promise.resolve().then(getPeopleQuery)
        return promise
      }

      const showError = t => {}

      const wrapperApp = mountWithStore(<People showError={showError} getPeople={loadData} />)

      // console.log(wrapperApp.debug());
      // We have CircularProgress appeared
      global.expect(wrapperApp.find('.loading')).to.have.length(1)

      // We set off CircularProgress, because demonstration app waits 2sec`s after data is loaded,
      // before it shows data
      wrapperApp.setState({
        runCircular: false
      })
      wrapperApp.instance().forceUpdate()
      wrapperApp.update()

      // CircularProgress is gone, we have empty table
      global.expect(wrapperApp.find('.EnhancedTable-root-15')).to.have.length(1)
      global.expect(wrapperApp.find('.loading')).to.have.length(0)
      // console.log(wrapperApp.debug());

      promise.then(() => {
        global.expect(wrapperApp.find('[alt]')).to.have.length(5)
        done()
      })
    })
  })
})

describe('as a user I can open dialog with person details', () => {
  it('as a user I can close dialog with person details', done => {
    const data = [
      {
        id: 6,
        first_name: 'Giustina',
        last_name: 'Edscer',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADPSURBVDjL3VIxCsJAEJyTaCmIYCFWfsMuviNPsAzkB/7E3gdob2+V1kKwsUmOmLu9c+8wASVocalcWGY5dmdmlxPWWoTEAIERNUWWZa0VIkJd16iqClJKn2VZ+iyKwmOe58L1itAVWgdpmtpfqs37q6dnB0mS2C+qxKonxtUnQT8OtvvbYTEdxl0NRBbaGChGpcBIeGjgfJHH3Wa59gRueDZut4ExFmTcIKcWjG4Q0JHhOvKk88kofrvB9a46lRRPKybQ2nii3m8Q/JX/gOAJ1Om5dnjvy8QAAAAASUVORK5CYII='
      },
      {
        id: 2,
        first_name: 'Gabriele',
        last_name: 'Palffrey',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAMxSURBVDjLTZNdaBxlFIafmd3tTmYnJU26TYgNbZoQ7GIMBH9iNQj1QimIFCL+EVEQQa+VXBQKBfVGIlL0QtDeBFEwFSlN0EaqgkaQ2tYG89OYxiaN2d1ssplNZmfONzv7ebEx9sC5fB/e874cQ2uNN26YwGngJCBAf+qEFu4ab9xoBi4BAfBy6oSeAzCKX5MA3k20vfTinvbX7vEXP8vPrrrzX9nnK0D3jn5qoPycmWlrycSTTYbMvvMXMNBwUi8buS84ZR0ZfNM69HxraXF08/byXPz71guOk4yoS1QB8EMT5Xsczw6UDx99hlhU8sozw8tAn6kE07AOWcX50c35hTlnsu1Lp71e0ej7yK0NvPkNnJJHS/0erjYP26uLk1asqa9R1x11lHDEWPiE/tCwP103Ow/+0vGN3WbHSZYj7u9spGGvicZgw434bXaNsC5GauM893qjktienk7q0guG1pq3z118P9Zw+K2OZocG0Tx7vJ2i61LwfNpaWwCoaPh8fIGqo8nmVyl48fc+fuWRUybAaqX1waqG6pbivq4myhVNIpFg4rtvKbkuoQiXJn4g07UfN+/jm/twVfJRgDiA+F53RRm4UqWpqQ6JNCnb5s7SEhOXf2Lqj+s88eRT7Dtgs1bw0Q0JxPe6dwHK9/DKSfyYgUS13vLuFinHYXstR+fBVm7duEZ2ZYW0FIndWWd/GDaeOXP5d3MHMFXaKmPEqyxmy0SGCaZF7wN9xEyDTCZDV1cXxWwWQpdMJkNPTw8i0mvunPBjyXXZCIRfr+VIJCFlJXis/xhhGFKpVAjDEK/sI0pQSiEiiAg1B4F30V3/Z3pmaZ1cMce50Tlur2xiSIhSandFBN8PdsUiUsvg57NvXHn49eGzHanND6YWK/b6lpDN5YlVTQ7cJQ5EiELRSilDa/0/AODDV4/drK+vt/OFYnR69GZsxrAxYxZPp2uAMAwxq4qqjgyR2p8ppWqAsbGxNHAhnU7jbbvudvHvj6Kqfhzolr1mo4gQBAFBEGittRGGIVprlFIYWmtGRkYc4ArQDvQODg7++Z+zoaGhG0qp7p0wJy3LeqhQKMR3gr36LzexzEk6GOJQAAAAAElFTkSuQmCC'
      },
      {
        id: 3,
        first_name: 'Kristofor',
        last_name: 'Freyn',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIQSURBVDjLjZPbbtNAFEVLvoNK/ENRv6W/guChbzwXJRJBDRIgVQglUjClddxUpCEF00RJE8fgXKBtZGzHud+vzWbOgE2gRdTSfrBn9jr7nPGsAFghsec20xrT+n90h8nj+pYAa+PxuD2bzS7n8zmuE1uH1+vdWoYsA9bJ3O/3MRgMXHU6HbRaLVSrVQ4xTRM+n8+FOOZbjHy/VCohnU4jmUwim81C13X0ej20223Yts0Bw+EQVMTv9/+E5HI5TyaTeZhKpRbNZpNvJFOj0YAkScjn8zxFrVa70hKfCTNvkHk0GoGkqiq63S5YO1yCIKBcLnNIvV7nBQzD+A1gZpGqKYrCo1JE0mQy4QDLshCLxfg8CEzzoP0uQJblCg2Geh/2WwiFQjw6GS4qOooXFl69OeQnQGBqj0AuIJF4XzHKu9BST9EzJeztBxGPx3FudZA4PUNKM7ATPsB0OuWpnIQugMUTbbMAw/yK/PckTvWPOLeLMCwbn5QznHzWIURivB0CkCiNC4hGoxu7EWGRN5I4+XaEY+0AcTUCtaigatexvfMaXwolnoBE5j8Aoih6gnsvHz1/+3hxXIhCLr3Dh8IhZC2GQCAANiNe1QE4cgHOj/Rg897m/pGAF8I2noWfICwFoRU09zj/1hUAvbCPi3/dg2t06QJ+Qe6yqANauImZ7e3x27sEWCXIDa6zI7r6qz8AeSLtQ3VwWP8AAAAASUVORK5CYII='
      },
      {
        id: 4,
        first_name: 'Kittie',
        last_name: 'Snar',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIhSURBVDjLY/j//z8DJZgsTV+9fAu+uHo8+GzvXECWAV+c3R//mTn9/ydLu4eka3ZyY/ts63T3k4Xt+4/GlqS74JONY+9Hc5tdH4wsmAmGgWv9xQKX2nMPnapOF4A1WzsEfjSzefLB0FwUHoi/szPX/05P/f0rOWk9ugHONWefzNl44X/B/L3/o7LXnn1h4fitN6i22Tx7W5tpxqYHxmnrChh+p6X+/rd10/+fsbF/f0REmiE0n7F3rDz5wb7s6Bu3gt3Vz80db69zTd1mlr11tUnGxt89Cw/8N0ha9YDhZ2LC+p8xMb9/hEdc+h4Ucu+br//JFXFNi5zKjz20KztiDzIMGFgzP+iZboQZbpSypsAgaeUjvfilqIEI9C9bf8rk3Wd8kz59sHV+BQysa8DA+vNe1+RreV94S96UiE9pff7/I1scPnlW6NWgBCLQvxKOVaeO2ZcfW2pbcogTGFgGwMD6+2/alP+rYhz+Na5O/L/lytT/F57t+t+/O+t/eL/uf/NsyR4G17oLBUD/Pgf69w3Qv6XILnqvbbT+nZre74RWlz8bL0/4v/HapP8g0LMn9X//nnSQAd8ZnKrPPJi85uJ/oH9f4opOn2rD/9uuzPmPDDZdmgoy4D+DQ8XxArvSww9sivYX4DLAMkf6e/eupP/tuxLAmtt3JiBcQEzqAypsCe7R+N+7KwVsM4gG8cFhQGwSBiruAOJPIGdD6Q6QOAAJO6JfeUJqowAAAABJRU5ErkJggg=='
      },
      {
        id: 5,
        first_name: 'Jacintha',
        last_name: 'Casarino',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADPSURBVCjPdZFNCsIwEEZHPYdSz1DaHsMzuPM6RRcewSO4caPQ3sBDKCK02p+08DmZtGkKlQ+GhHm8MBmiFQUU2ng0B7khClTdQqdBiX1Ma1qMgbDlxh0XnJHiit2JNq5HgAo3KEx7BFAM/PMI0CDB2KNvh1gjHZBi8OR448GnAkeNDEDvKZDh2Xl4cBcwtcKXkZdYLJBYwCCFPDRpMEjNyKcDPC4RbXuPiWKkNABPOuNhItegz0pGFkD+y3p0s48DDB43dU7+eLWes3gdn5Y/LD9Y6skuWXcAAAAASUVORK5CYII='
      },
      {
        id: 6,
        first_name: 'Charita',
        last_name: 'Irvine',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJtSURBVDjLjZPLa1NREIe/m1dr1aY22tb4qEkEX4hYaKG+7UIFS5auBOlG6bbgHyC4EHEhKCjiVhBcaVWsBSVgikLF2iqxPqI1llrTxiba9N6Tc+64CAaDFRw4DDNwvt/MOTOICP9zpjo7ZbG8h/+0/p6eRfOWiFSCVColxhi01vz2Wmuy2SzJZJJwOEwsFiMej1u/7/j+pJVKJaLRKAB/gkWErq4uRISBgYGqCqoAWmsAHo+XOzMCxgVXXIyBI1s1juP8G6CUQkRoa/m+6EOq7Cgd3vu8v5OR+ZkElj15YlHA0y8rcMVFu+UKtIFmfY+dnhe0bD9OMLKTuY+bePPw+vm/AADtq2eqlK1CgqB/gqX1u8l9SBGwFMvrW1gWWh+sAjiOg4iQ+NSIEUG7ENJD7A+nCUYO40zdJFBnMTHyhpKtZu2Fn7uqAHkzh4iwZ122rF54RqNvjGAsjj15FU9A41/WSm1xbLr403ekvW/wbWWQTj/t3fC6YRgRwXVd7t84w1L9kuDGOPbkFTz+EqqwgczjZ6gfxe62vsGRql9wXTfi8XtJ5ceo+/yOg+2raNp2FPX1Ot6AYOdb+fbkOdeCIT54fev7YRgoj3Jf8lQ9xnoU8q9kKHGZgJ3GsI6psUu42Mzn1jA99IoLfi8NzTG0MhcOXezaXgEYbXodpeRddpzW6Rxb95ykJvOEt7eTjAzkeNGf5IG7mYJaQq4wg9Y6bLR7ttKCuVV7DsACxtU06vVdIjv2ks/Okh5OMVp3gMLXIo1WE7OZ76xVrTU1qjb+1zIBHNvXuNAWbfZ1bIkgfmdo4Vu2p/vcaPpfW/oLvSN/oHdKKLQAAAAASUVORK5CYII='
      },
      {
        id: 1,
        first_name: 'Kittie',
        last_name: 'Snar',
        avatar:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIhSURBVDjLY/j//z8DJZgsTV+9fAu+uHo8+GzvXECWAV+c3R//mTn9/ydLu4eka3ZyY/ts63T3k4Xt+4/GlqS74JONY+9Hc5tdH4wsmAmGgWv9xQKX2nMPnapOF4A1WzsEfjSzefLB0FwUHoi/szPX/05P/f0rOWk9ugHONWefzNl44X/B/L3/o7LXnn1h4fitN6i22Tx7W5tpxqYHxmnrChh+p6X+/rd10/+fsbF/f0REmiE0n7F3rDz5wb7s6Bu3gt3Vz80db69zTd1mlr11tUnGxt89Cw/8N0ha9YDhZ2LC+p8xMb9/hEdc+h4Ucu+br//JFXFNi5zKjz20KztiDzIMGFgzP+iZboQZbpSypsAgaeUjvfilqIEI9C9bf8rk3Wd8kz59sHV+BQysa8DA+vNe1+RreV94S96UiE9pff7/I1scPnlW6NWgBCLQvxKOVaeO2ZcfW2pbcogTGFgGwMD6+2/alP+rYhz+Na5O/L/lytT/F57t+t+/O+t/eL/uf/NsyR4G17oLBUD/Pgf69w3Qv6XILnqvbbT+nZre74RWlz8bL0/4v/HapP8g0LMn9X//nnSQAd8ZnKrPPJi85uJ/oH9f4opOn2rD/9uuzPmPDDZdmgoy4D+DQ8XxArvSww9sivYX4DLAMkf6e/eupP/tuxLAmtt3JiBcQEzqAypsCe7R+N+7KwVsM4gG8cFhQGwSBiruAOJPIGdD6Q6QOAAJO6JfeUJqowAAAABJRU5ErkJggg=='
      }
    ]

    const getPeopleQuery = () => Promise.resolve(data)
    let promise

    const loadData = () => {
      promise = Promise.resolve().then(getPeopleQuery)
      return promise
    }

    const showError = t => {}

    const wrapperApp = mountWithStore(<People showError={showError} getPeople={loadData} />)

    global.expect(wrapperApp.find('.loading')).to.have.length(1)

    wrapperApp.setState({
      runCircular: false
    })
    wrapperApp.instance().forceUpdate()
    wrapperApp.update()

    // CircularProgress is gone, we have empty table
    global.expect(wrapperApp.find('.EnhancedTable-root-15')).to.have.length(1)
    global.expect(wrapperApp.find('.loading')).to.have.length(0)

    promise.then(() => {
      const tableWrapper = wrapperApp
        .find('WithStyles(TableBody)')
        .at(0)
        .dive()
      // console.log(tableWrapper.debug());

      const firstDataTableRow = tableWrapper
        .find('WithStyles(TableRow)')
        .at(0)
        .dive()
      //  console.log(firstDataTableRow.prop('children'))
      //  console.log(firstDataTableRow.props())

      // Issue solution: https://github.com/airbnb/enzyme/issues/1081

      firstDataTableRow.simulate('click')
      firstDataTableRow.simulate('click')

      const openPersonWindowButtonContainer = wrapperApp
        .find('WithStyles(EnhancedTableToolbar)')
        .at(0)
        .dive()

      openPersonWindowButtonContainer.setProps({
        numSelected: 1
      })
      openPersonWindowButtonContainer.instance().forceUpdate()
      openPersonWindowButtonContainer.update()

      const openPersonWindowButtonControl = openPersonWindowButtonContainer
        .dive()
        .find('PersonDialog')
        .at(0)
        .dive()
      openPersonWindowButtonControl.setProps({
        selected: openPersonWindowButtonContainer.prop('selected')
      })
      openPersonWindowButtonControl.instance().forceUpdate()
      openPersonWindowButtonControl.update()

      const openPersonWindowButton = openPersonWindowButtonControl
        .find('WithStyles(IconButton)[aria-label="Edit"]')
        .at(0)
        .dive()

      openPersonWindowButton.simulate('click')
      openPersonWindowButton.simulate('click')

      global
        .expect(openPersonWindowButtonControl.find('WithStyles(Dialog)[open=true]'))
        .to.have.length(1)

      openPersonWindowButtonControl.setState({
        // data : data, //loadData,
        runCircular: false,
        selected: openPersonWindowButtonContainer.prop('selected')
      })
      openPersonWindowButtonControl.instance().forceUpdate()
      openPersonWindowButtonControl.update()

      const editedUserId = openPersonWindowButtonControl.find('input[name="id"]')

      global
        .expect(editedUserId.prop('value'))
        .to.equal(openPersonWindowButtonContainer.prop('selected').id)

      const closeDialogButton = openPersonWindowButtonControl.find(
        'WithStyles(Button)[color="primary"]'
      )

      closeDialogButton.simulate('click')
      closeDialogButton.simulate('click')

      // DialogIsClosed
      global
        .expect(openPersonWindowButtonControl.find('WithStyles(Dialog)[open=false]'))
        .to.have.length(1)

      done()
    })
  })
})
