import React from 'react'
import ErrorWindow from '../views/Components/ErrorWindow'
import * as testHelper from '../utils/tests/helpers'
import App from '../App'


it('renders without crashing', () => {
  let showError = (t)=>{}
  global.shallow(<ErrorWindow open={true} title='Error window test' message='Error content test' showError={showError}/>);
});

it('renders the wrapper with ErrorWindow', () => {
  let showError = (t)=>{}
  const wrapper = global.shallow(<ErrorWindow open={true} title='Error window test' message='Error content test' showError={showError}/>);
  //console.log(wrapper.debug())
  global.expect(wrapper.find('#alert-dialog-title')).to.have.length(1);
  global.expect(wrapper.find('#alert-dialog-description2')).to.have.length(1);
});


describe('App Component', () => {
  it('renders the people wrapper', () => {
    const wrapper = global.shallow(<App />);
    global.expect(wrapper.find(ErrorWindow)).to.have.length(1);
  });
});

it('exists in people wrapper', () => {
  let showError = (t)=>{}
  const wrapper = global.shallow(<ErrorWindow open={true} title='Error window test' message='Error content test' showError={showError}/>);
  //console.log(wrapper.debug())
  global.expect(wrapper.find('#alert-dialog-title')).to.have.length(1);
  global.expect(wrapper.find('#alert-dialog-description2')).to.have.length(1);
});

it('props in people wrapper', () => {
  let showError = (t)=>{}
  const wrapper = global.shallow(<ErrorWindow open={true} title='Error window test' message='Error content test' showError={showError}/>);
  //console.log(wrapper.debug())
  global.expect(wrapper.find('#alert-dialog-title')).to.have.length(1);
  global.expect(wrapper.find('#alert-dialog-description2')).to.have.length(1);
});

it('passes all props to people wrapper', () => {
  let showError = (t)=>{}
  const wrapper = global.shallow(<ErrorWindow open={true} title='Error window test' message='Error content test' showError={showError}/>);
  //console.log(wrapper.children().props().open)
  expect(wrapper.children().props().open).to.equal(true);
  // wrapper.children().setState({ open: false });
  // expect(wrapper.children().props().open).to.equal(false);
});
