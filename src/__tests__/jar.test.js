import React from 'react'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'
import PropTypes from 'prop-types'
import createHistory from 'history/createBrowserHistory'
import { Provider } from 'react-redux'
import Jar from '../views/bank/jar'
import CurrencySelect from '../views/bank/currencySelector'
import BankSelect from '../views/bank/bankSelector'
import * as testHelper from '../utils/tests/helpers'
import storeConfig from '../store/index'

const mountWithStore = (component, store) => {
  const muiTheme = getMuiTheme(darkBaseTheme)
  const context = {
    store,
    muiTheme
  }

  const childContextTypes = { muiTheme: PropTypes.object, store: PropTypes.object }

  return global.shallow(component, { context, childContextTypes }).dive({ childContextTypes })
}

describe('JAR Component', () => {
  it('click to added value, add transaction works', () => {
    const history = createHistory()
    const store = storeConfig(history)
    const wrapper = mountWithStore(<Jar store={store} />)
    global.expect(wrapper.find(BankSelect)).to.have.length(3)
  })
})

describe('JAR Component', () => {
  it('renders the CurrencySelect component', () => {
    const history = createHistory()
    const store = storeConfig(history)
    const wrapper = mountWithStore(<Jar store={store} />)
    global.expect(wrapper.find(CurrencySelect)).to.have.length(2)
  })
})

describe('JAR Component', () => {
  it('renders the BankSelect 2 component', () => {
    const history = createHistory()
    const store = storeConfig(history)
    const wrapper = mountWithStore(<Jar store={store} />)
    global.expect(wrapper.find(BankSelect)).to.have.length(3)

    const refTextBox = wrapper.instance().textInput1

    refTextBox.current = { value: 10 }

    const firstInput = wrapper.find('#testValue1')
    firstInput.value = 10
    firstInput.simulate('change', firstInput)

    const button = wrapper.find('[className="btn btn-primary"]').at(0)

    button.simulate('click')

    const state = store.getState()

    global.expect(state.jar_bank.banks.PKO.operations[0].amount).to.equal(firstInput.value)
  })
})
