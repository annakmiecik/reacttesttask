import { render, unmountComponentAtNode } from 'react-dom'
import React from 'react'
import { Route, Link, MemoryRouter } from 'react-router-dom'
import { Simulate } from 'react-addons-test-utils'
import App from '../App'
import * as testHelper from '../utils/tests/helpers'
import { renderTestSequence } from '../utils/tests/renderTestSequence'

// the actual test!
it('as a user I should be able to access person details by entering URL', done => {
  jest.setTimeout(30000)
  renderTestSequence({
    // tell it the subject you're testing
    subject: App,

    // and the steps to execute each time the location changes
    steps: [
      // initial render
      ({ history, div }) => {
        console.log(div)
        history.push('/people/1')
      },

      // second render from new location
      ({ location, div }) => {
        global.expect(location.pathname).to.equal('/people/1')
        done()
      }
    ]
  })
})
