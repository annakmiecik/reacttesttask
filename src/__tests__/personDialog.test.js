import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import CircularProgress from 'material-ui/CircularProgress'
import PersonDialog from '../views/personDialog'
import App from '../App'
import * as testHelper from '../utils/tests/helpers'

it('renders without crashing', () => {
  const showError = t => true
  global.shallow(<PersonDialog open showError={showError} />)
})

// it("openByUrl", () => {
//   const wrapperApp = global.mount(
//       <MemoryRouter initialEntries={["/person/1"]}>
//         <App />
//       </MemoryRouter>
//     );
//   const wrapper = wrapperApp.find(PersonDialog).first();
//   console.log(wrapperApp)
//   global.expect(wrapper.find(CircularProgress)).to.have.length(1);
// });
